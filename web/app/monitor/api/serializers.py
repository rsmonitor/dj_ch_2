from ..models import (
    Port, Station, CommandsSequence, Operator, Group,
    Command, PaidLicense, ListOfCommands
)
from rest_framework import serializers, viewsets
from rest_framework.exceptions import NotAuthenticated
from django.db.models import ObjectDoesNotExist
from django.contrib.auth.models import AnonymousUser, User
from rest_framework import routers
from rest_framework.response import Response
import json


class CommandSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Command
        fields = (
            'name',
            'cmd',
        )


class CommandViewSet(viewsets.ModelViewSet):
    queryset = Command.objects.all()
    serializer_class = CommandSerializer


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = (
            'username',
            'email',
            # 'pk',
        )


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class OperatorSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Operator
        fields = (
            # 'pk',
            'group',
            'user',
        )


class OperatorViewSet(viewsets.ModelViewSet):
    queryset = Operator.objects.all()
    serializer_class = OperatorSerializer


class ListOfCommandsSerializer(serializers.HyperlinkedModelSerializer):
    commands = CommandSerializer(source='command_set', many=True)

    class Meta:
        model = ListOfCommands
        fields = (
            'name',
            'commands'
        )


class ListOfCommandsViewSet(viewsets.ModelViewSet):
    queryset = ListOfCommands.objects.all()
    serializer_class = ListOfCommandsSerializer


class CommandsSequenceSerializer(serializers.HyperlinkedModelSerializer):
    list_of_commands = ListOfCommandsSerializer(read_only=True, many=True)

    class Meta:
        model = CommandsSequence
        fields = (
            'name',
            'sequence',
            'list_of_commands'
        )


class CommandsSequenceViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        return CommandsSequence.objects.all()

    def create(self, request, *args, **kwargs):
        # todo: check user permissions
        station_id = request.data.get('station', None)
        seq_name = request.data.get('name', None)
        seq_data = request.data.get('sequence', None)

        try:
            station = Station.objects.get(pk=station_id)
            CommandsSequence(
                station=station,
                name=seq_name,
                sequence=json.dumps(seq_data)
            ).save()
        except Exception as e:
            raise ObjectDoesNotExist(e)
        else:
            return Response(data={'status': 200})

    queryset = CommandsSequence.objects.all()
    serializer_class = CommandsSequenceSerializer


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)


class GroupViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        user = self.request.user
        return Group.objects.all()

    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class PortSerializer(serializers.HyperlinkedModelSerializer):
    lists_of_commands = ListOfCommandsSerializer(
        source='listofcommands_set',
        many=True
    )

    class Meta:
        model = Port
        fields = (
            'pk',
            'baudrate',
            'bytesize',
            'dsrdtr',
            'exclusive',
            'inter_byte_timeout',
            'parity',
            'port_os_id',
            'rtscts',
            'stopbits',
            'timeout',
            'write_timeout',
            'xonxoff',
            'lists_of_commands'
        )


class PortSerializerBasic(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Port
        fields = (
            'baudrate',
            'bytesize',
            'dsrdtr',
            'exclusive',
            'inter_byte_timeout',
            'parity',
            'port_os_id',
            'rtscts',
            'stopbits',
            'timeout',
            'write_timeout',
            'xonxoff',
        )


class PortViewSet(viewsets.ModelViewSet):
    queryset = Port.objects.all()
    serializer_class = PortSerializer

    def create(self, request, *args, **kwargs):
        # todo: check user permissions
        new_config = request.data.get('new_port_config', None)
        pk = new_config.pop('pk')
        Port.objects.filter(pk=pk).update(**new_config)
        return Response(data={'status': 200})


class StationSerializerSimple(serializers.HyperlinkedModelSerializer):
    ports = PortSerializerBasic(source='port_set', many=True)

    class Meta:
        model = Station
        fields = (
            'pk',
            'ports',
        )


class StationSerializer(serializers.HyperlinkedModelSerializer):
    ports = PortSerializer(source='port_set', many=True)
    sequences = CommandsSequenceSerializer(
        source='commandssequence_set',
        many=True
    )

    class Meta:
        model = Station
        fields = (
            'pk',
            'name',
            'ip',
            'os',
            'ports',
            'sequences',
        )


class StationViewSet(viewsets.ModelViewSet):
    def get_object(self):
        pk = self.kwargs.get('pk', None)

        if pk == 'agent':
            params = self.request.query_params
            license_hash = params.get('license_hash', None)

            # Request come from Agent (station), so
            # identification done by license_hash
            if license_hash:
                try:
                    paid_license = PaidLicense.objects.get(license_hash=license_hash)
                except ObjectDoesNotExist:
                    raise NotAuthenticated('Wrong license hash')

                if not paid_license.is_active():
                    raise NotAuthenticated('The license has expired')

                try:
                    customer = paid_license.customer
                except ObjectDoesNotExist:
                    raise NotAuthenticated('No customer present in data base')

                group_name = params.get('group_name', None)

                try:
                    group = Group.objects.get(customer=customer, name=group_name)
                    return group.station_set.get(name=params.get('station_name', None))
                except ObjectDoesNotExist:
                    raise NotAuthenticated(f'Wrong group name ({group_name})')
        else:
            user = self.request.user

            if user == AnonymousUser():
                raise NotAuthenticated('Bad license or user+password')
            else:
                group = Operator.objects.get(user=user).group
                return Station.objects.get(group=group, pk=pk)

    def get_serializer_class(self):
        pk = self.kwargs.get('pk', None)

        if pk == 'agent':
            return StationSerializerSimple
        else:
            return StationSerializer

    queryset = Station.objects.all()
    serializer_class = StationSerializer


router = routers.DefaultRouter()
router.register(r'stations', StationViewSet)
router.register(r'commands_sequences', CommandsSequenceViewSet)
router.register(r'ports', PortViewSet)
