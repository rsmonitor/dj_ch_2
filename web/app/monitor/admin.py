from django.contrib import admin
from .models import (
    PaidLicense,
    Customer,
    Group,
    Operator,
    Station,
    Port,
    Command,
    ListOfCommands,
    CommandsSequence
)

admin.site.register(PaidLicense)
admin.site.register(Customer)
admin.site.register(Group)
admin.site.register(Operator)
admin.site.register(Station)
admin.site.register(Port)
admin.site.register(ListOfCommands)
admin.site.register(Command)
admin.site.register(CommandsSequence)
