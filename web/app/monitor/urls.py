from . import views
from django.conf.urls import url, include
from .api.serializers import router
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
   openapi.Info(
      title="Monitor API",
      default_version='v1',
      description="Test description",
   ),
   public=True,
)

urlpatterns = [
    url(r'^$', views.operator_home, name='index'),
    url(r'^operator_home/', views.operator_home, name='operator_home'),
    url(r'^group/(?P<group_id>.+)/$', views.group, name='group'),
    url(r'^station/(?P<station_id>.+)/$', views.station, name='station'),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^swagger(?P<format>\.json|\.yaml)/$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
]
