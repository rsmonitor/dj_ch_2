from asgiref.sync import async_to_sync
from channels.db import database_sync_to_async
from channels.generic.websocket import WebsocketConsumer
from monitor.models import User, Operator, PaidLicense
from django.contrib.auth.models import AnonymousUser
from django.db.models import ObjectDoesNotExist
import json
import time


def timer(f):
    def inner(*args, **kwargs):
        time_0 = time.time()
        ret = f(*args, **kwargs)
        print(f'{f.__name__}, {(time.time() - time_0) * 1e6}mks')
        return ret

    return inner


class MonitorConsumer(WebsocketConsumer):
    def authorize(self):
        # FIXME
        return True
        if self.endpoint == 'agent':
            license_hash = self.headers.get('license-hash', None)

            try:
                pl = PaidLicense.objects.get(license_hash=license_hash)
                return pl.is_active()
            except ObjectDoesNotExist:
                return False
            except Exception as e:
                print(e)
                return False

        elif self.endpoint == 'gui':
            user = self.scope['user']

            return True if Operator(user=user) is not AnonymousUser else False

    def connect(self):
        self.headers = {
            str(k, encoding='utf8'): str(v, encoding='utf8')
            for k, v in self.scope['headers']
        }
        # workaround: can't add custom headers to a browser's WebSocket
        self.endpoint = 'agent' if self.headers.get('endpoint', None) else 'gui'
        self.station_id = self.scope['url_route']['kwargs']['station_id']

        if not self.authorize():
            return

        async_to_sync(self.channel_layer.group_add)(self.station_id, self.channel_name)
        self.accept()
        self.channel_layer.flush()

        group_announcement = {
            'sender': 'backend',
            'type': 'chat_message',
            'data': {
                'connection_state': {
                    'endpoint': self.endpoint,
                    'state': True,
                }
            }
        }
        async_to_sync(
            self.channel_layer.group_send
        )(
            self.station_id, group_announcement
        )

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_send)(
            self.station_id,
            {
                'sender': 'backend',
                'type': 'chat_message',
                'data': {
                    'connection_state': {
                        'endpoint': self.endpoint,
                        'state': False,
                        'close_code': close_code
                    }
                }
            }
        )
        async_to_sync(
            self.channel_layer.group_discard
        )(
            self.station_id,
            self.channel_name
        )
        self.channel_layer.flush()

    def receive(self, text_data=None, bytes_data=None):
        if text_data:
            text_data_json = json.loads(text_data)
            text_data_json['type'] = 'chat_message'
            async_to_sync(
                self.channel_layer.group_send
            )(
                self.station_id, text_data_json
            )
        elif bytes_data:
            async_to_sync(self.channel_layer.group_send)(self.station_id, {
                'bytes_data': bytes_data,
                'type': 'binary_message'
            })

    @timer
    def chat_message(self, event):
        sender = event['sender']

        if sender == self.endpoint:
            return

        # Do not inform endpoint about its connect state
        elif sender == 'backend':
            if self.endpoint == event['data']['connection_state']['endpoint']:
                return

        event.pop('type')
        self.send(text_data=json.dumps(event))
        self.channel_layer.flush()

    def binary_message(self, event):
        self.send(bytes_data=event['bytes_data'])
