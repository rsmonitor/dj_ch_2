import os
import random
from ..models import *


def create_random_accounts():
    cur_dir = os.path.dirname(os.path.realpath(__file__))

    with open(os.path.join(cur_dir, 'tests', 'first-names.txt'), 'r') as _:
        f_names = _.read().split()

    with open(os.path.join(cur_dir, 'tests', 'last-names.txt'), 'r') as _:
        l_names = [l_name.lower().capitalize() for l_name in _.read().split()]

    with open(os.path.join(cur_dir, 'tests', 'free_email_provider_domains.txt'), 'r') as _:
        domains = _.read().split()

    users = []

    for i in range(1000):
        first_name = random.choice(f_names)
        first_name_lower = first_name.lower()
        last_name = random.choice(l_names)
        last_name_lower = last_name.lower()
        domain = random.choice(domains)
        email = f'{first_name_lower}_{last_name_lower}@{domain}')
        users.append(
            User(
                username=f'{first_name_lower}_{last_name_lower}),
                first_name=first_name,
                last_name=last_name,
                email=email,
                password='123456'
            )
        )

    status = User.objects.bulk_create(users)
    return status


if __name__ == '__main__':
    create_random_accounts()
