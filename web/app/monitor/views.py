# TODO: REST API for non-view responses
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Station, Operator, Group, Customer
from django.db.models import ObjectDoesNotExist
from django.http import Http404


@login_required(login_url='/')
def operator_home(request):
    print(request.user)
    try:
        operator = Operator.objects.get(user=request.user)
        working_group = operator.group
        return render(
            request,
            'monitor/operator.html',
            {
                'user': request.user,
                'groups': [working_group]
            }
        )
    except ObjectDoesNotExist:
        pass

    try:
        customer = Customer.objects.get(manager=request.user)
        print(customer)
        return render(
            request,
            'monitor/operator.html',
            {
                'user': customer,
                'groups': None
            }
        )
    except ObjectDoesNotExist:
        raise Http404('No customer')


@login_required(login_url='/')
def group(request, group_id):
    print('\n\n', request.user, '\n\n')
    operator = Operator.objects.get(user=request.user)
    working_group = Group.objects.get(id=group_id)
    stations = working_group.station_set.all()

    return render(
        request,
        'monitor/group.html',
        {
            'working_group': working_group,
            'operator': operator.user,
            'stations': stations
        }
    )


@login_required(login_url='/')
def station(request, station_id):
    try:
        st = Station.objects.get(id=station_id)
    except ObjectDoesNotExist:
        raise Http404('The station does not exist')

    return render(
        request,
        'monitor/station.html',
        {
            'station': st
        }
    )


@login_required(redirect_field_name='next')
def index(request):
    return render(
        request,
        'monitor/index.html',
        {}
    )
