const path = require('path');
let webpack = require('webpack');
let BUILD_DIR = path.resolve(__dirname, '../../static/monitor/gui/dist/');
let APP_DIR = path.resolve(__dirname, 'src/');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
let LiveReloadPlugin = require('webpack-livereload-plugin');


module.exports = {
    entry: APP_DIR + '/index.js',
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js'
    },
    plugins: [
        new ExtractTextPlugin('css/station-monitor.css'),
        new LiveReloadPlugin(),
    ],
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    // fallback: 'style-loader',
                    use: [
                        "css-loader",
                        "sass-loader",
                    ]
                })
            },
        ],

    },
    resolve: {
        extensions: [".js", ".jsx", ".scss"],
    }
};
