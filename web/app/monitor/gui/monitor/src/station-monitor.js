import React from "react";
import { fetchFromUrl, postToUrl } from "./api-fetch/api-fetch";
import { logger, createWebSocket, errorHandler } from "./utils";
import TopBar from "./top-bar";
import DashBoards from "./dashboards";
import MinimizedPortDashboards from "./toolbar/minimized-port-dashboards";

class StationMonitor extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      stationId: props.stationId,
      portsConfig: undefined,
      stationSettings: undefined,
      devices: {},
      isAgentConnected: false,
      blinkingDevices: {},
      minimizedDashBoards: {},
      maximizedDashBoards: {},
      isMonitoringAllPorts: true,
      isBackendAvailable: undefined,
      sequences: undefined,
      newSequence: Array(),
      newSequenceName: undefined,
      isSaveSequenceDialogEnabled: false,
      isNewSequenceNameDialogActive: false,
      isRecordingSequence: false,
      isRecordNewSequenceDisabled: false,
      isRunningSequence: false,
      sequenceToRun: "",
      endlessLoop: false,
      schemes: {},
    };

    this.sendToBackend = this.sendToBackend.bind(this);
    this.setStationConfig = this.setStationConfig.bind(this);
    this.onWebSocketMessage = this.onWebSocketMessage.bind(this);
    this.updateMessageHistory = this.updateMessageHistory.bind(this);
    this.clearHistory = this.clearHistory.bind(this);
    this.toggleMonitoringAllPorts = this.toggleMonitoringAllPorts.bind(this);
    this.toggleMonitoring = this.toggleMonitoring.bind(this);
    this.clearHistoryAll = this.clearHistoryAll.bind(this);
    this.setBackendConnectionStatus =
      this.setBackendConnectionStatus.bind(this);
    this.maximizeDashBoard = this.maximizeDashBoard.bind(this);
    this.minimizeDashBoard = this.minimizeDashBoard.bind(this);
    this.toggleRecordingSequence = this.toggleRecordingSequence.bind(this);
    this.updateRecordedSequence = this.updateRecordedSequence.bind(this);
    this.toggleRunningSequence = this.toggleRunningSequence.bind(this);
    this.setSequenceToRun = this.setSequenceToRun.bind(this);
    this.discardNewSequence = this.discardNewSequence.bind(this);
    this.saveNewSequence = this.saveNewSequence.bind(this);
    this.setNewSequenceName = this.setNewSequenceName.bind(this);
    this.deleteCommandFromSequence = this.deleteCommandFromSequence.bind(this);
    this.addPauseToSequence = this.addPauseToSequence.bind(this);
    this.setPauseValue = this.setPauseValue.bind(this);
    this.closeRunningSequence = this.closeRunningSequence.bind(this);
    this.toggleEndlessLoop = this.toggleEndlessLoop.bind(this);
    this.stopBlinking = this.stopBlinking.bind(this);
    this.getSchemes = this.getSchemes.bind(this);
    this.getStationConfig = this.getStationConfig.bind(this);
    this.setStationConfig = this.setStationConfig.bind(this);
    this.toggleEditorMode = this.toggleEditorMode.bind(this);
  }

  toggleEndlessLoop() {
    this.setState({
      endlessLoop: !this.state.endlessLoop,
    });
  }

  saveNewSequence() {
    const url = `/monitor/api/commands_sequences/`;
    postToUrl(
      url,
      {
        name: this.state.newSequenceName,
        sequence: this.state.newSequence,
        station: this.state.stationId,
      },
      () => {
        alert(`New sequence (${this.state.newSequenceName}) saved`);
      },
      (e) => {
        alert(
          `Failed to save the new sequence (${this.state.newSequenceName})\n${e}`
        );
      }
    );
    this.discardNewSequence();
    this.getStationConfig();
  }

  setNewSequenceName(newSequenceName) {
    this.setState({
      newSequenceName: newSequenceName,
      isNewSequenceNameDialogActive: false,
    });
  }

  discardNewSequence() {
    this.setState({
      newSequence: Array(),
      newSequenceName: undefined,
      isRecordingSequence: false,
      isNewSequenceNameDialogActive: false,
    });
  }

  toggleRunningSequence() {
    this.setState({
      isRunningSequence: !this.state.isRunningSequence,
    });
  }

  closeRunningSequence() {
    this.setState({
      sequenceToRun: "",
      isRunningSequence: false,
    });
  }

  toggleEditorMode() {
    let sequences = this.state.sequences;
    let sequenceToRun = this.state.sequenceToRun;
    console.log(`\n\n\n${sequences[sequenceToRun]}\n\n\n`);
  }

  toggleRecordingSequence() {
    const isRecordingSequence = this.state.isRecordingSequence;

    this.setState({
      isRecordingSequence: !isRecordingSequence,
      isNewSequenceNameDialogActive: !isRecordingSequence,
    });
  }

  setSequenceToRun(sequence) {
    this.setState({
      sequenceToRun: sequence,
    });
  }

  maximizeDashBoard(deviceId) {
    const maximizedDashBoards = this.state.maximizedDashBoards;
    const minimizedDashBoards = this.state.minimizedDashBoards;
    maximizedDashBoards[deviceId] = minimizedDashBoards[deviceId];
    delete minimizedDashBoards[deviceId];

    this.setState({
      maximizedDashBoards: maximizedDashBoards,
      minimizedDashBoards: minimizedDashBoards,
    });
  }

  minimizeDashBoard(deviceId) {
    const maximizedDashBoards = this.state.maximizedDashBoards;
    const minimizedDashBoards = this.state.minimizedDashBoards;
    minimizedDashBoards[deviceId] = maximizedDashBoards[deviceId];
    delete maximizedDashBoards[deviceId];

    this.setState({
      maximizesDashBoards: maximizedDashBoards,
      minimizedDashBoards: minimizedDashBoards,
    });
  }

  setBackendConnectionStatus(status) {
    if (!status) {
      this.setState({
        devices: {},
        minimizedDevices: {},
        maximizedDevices: {},
        sequenceToRun: "",
        isRunningSequence: false,
      });
    }

    this.setState({
      isBackendAvailable: status,
    });
  }

  processPortsMonitorMessage(message) {
    switch (message.agent) {
      case "connected":
        this.setState({
          isAgentConnected: true,
        });
        break;
      case "disconnected":
        this.setState({
          isAgentConnected: false,
        });
        break;
    }
    switch (message.command) {
      case "update_attached_ports":
        console.log(message.ports);
        const oldDevices = this.state.devices;
        const newDevices = {};

        for (let deviceId in message.ports) {
          if (!oldDevices.hasOwnProperty(deviceId)) {
            newDevices[deviceId] = {
              history: Array(),
              monitor: true,
            };
          } else {
            newDevices[deviceId] = oldDevices[deviceId];
          }
        }

        const maximizedDashBoards = {};

        for (let deviceId in newDevices) {
          maximizedDashBoards[deviceId] = {};
        }

        this.setState({
          devices: newDevices,
          maximizedDashBoards: maximizedDashBoards,
        });
        break;

      case "update_history":
        this.updateMessageHistory(message);
        break;
    }
  }

  processBackendMessage(message) {
    console.log("processBackendMessage");
    console.log(message);

    if (message.agent === "disconnected") {
      this.setState({
        minimizedDashBoards: {},
        maximizedDashBoards: {},
        devices: {},
        isAgentConnected: false,
      });
    } else if (message.agent === "connected") {
      this.setState({
        isAgentConnected: true,
      });
    }
  }

  onWebSocketMessage(message) {
    const data = JSON.parse(message.data);
    const sender = data.sender;
    console.log(data);

    switch (sender) {
      case "backend":
        this.processBackendMessage(data);
        break;
      case "agent":
        this.processPortsMonitorMessage(data.data);
        break;
      case "gui":
        //Ignore echo-messages
        return;
    }
  }

  setStationConfig(stationConfig) {
    const sequencesObj = {};
    const sequences = stationConfig.sequences;

    for (let i = 0; i < sequences.length; i++) {
      let sequence = sequences[i];
      sequencesObj[sequence.name] = JSON.parse(sequence.sequence);
    }

    this.setState({
      portsConfig: stationConfig.ports,
      sequences: sequencesObj,
    });
  }

  getStationConfig() {
    const url = `/monitor/api/stations/${this.state.stationId}/`;
    fetchFromUrl(url, this.setStationConfig, errorHandler);
  }

  getSchemes() {
    const url = "/monitor/swagger.json/";
    fetchFromUrl(
      url,
      (data) => this.setState({ schemes: data.definitions }),
      console.log
    );
  }

  componentDidMount() {
    const wsScheme = window.location.protocol === "https:" ? "wss" : "ws";
    const url = `${wsScheme}://${
      window.location.host
    }/ws${window.location.pathname.replace("/station", "")}`;
    this.webSocket = createWebSocket(
      url,
      this.onWebSocketMessage,
      this.setBackendConnectionStatus
    );
    this.getStationConfig();
    this.getSchemes();
    const topOffset = document.getElementById("top-bar").clientHeight;
    document
      .getElementById("dashboards")
      .style.setProperty("padding-top", topOffset + 25 + "px");
  }

  updateRecordedSequence(msg) {
    const sequence = this.state.newSequence;
    sequence.push({
      device_id: msg.device_id,
      command: msg.command,
    });
    this.setState({
      sequence: sequence,
    });
  }

  sendToBackend(msg) {
    msg.sender = "gui";
    const request = msg.request;

    try {
      if (request === "send_command_to_device") {
        if (this.state.isRecordingSequence) {
          this.updateRecordedSequence(msg);
        } else {
          this.webSocket.send(JSON.stringify(msg));
        }

        this.updateMessageHistory(msg);
      } else if (request === "toggle_port_state") {
        this.webSocket.send(JSON.stringify(msg));
      }
    } catch (e) {
      console.log(e);
    }
  }

  clearHistoryAll() {
    const devices = this.state.devices;

    for (let deviceId in devices) {
      devices[deviceId].history = Array();
    }

    this.setState({
      devices: devices,
    });
  }

  updateMessageHistory(message) {
    if (this.state.isRecordingSequence) {
      return;
    }

    const devices = this.state.devices;
    const timeStamp = `${new Date()
      .toISOString()
      .replace("T", ", ")
      .replace("Z", "")}`;

    switch (message.sender) {
      case "gui":
        const deviceId = message["device_id"];
        const deviceHistory = devices[deviceId].history;
        deviceHistory[deviceHistory.length] = {
          type: "sent",
          frame: message.command,
          timeStamp: timeStamp,
        };
        devices[deviceId].history = deviceHistory;
        break;
      case "ports_monitor":
        const devicesData = message.data;

        for (let deviceId in devicesData) {
          const deviceHistory = devices[deviceId].history;
          deviceHistory[deviceHistory.length] = {
            type: "received",
            frame: devicesData[deviceId]
              .replace("\r", "&crarr;<br/>")
              .replace("\n", "&crarr;<br/>"),
            timeStamp: timeStamp,
          };
          devices[deviceId].blinking = true;
        }
        break;
      default:
        logger("WRONG DATA RECEIVED", message);
    }

    this.setState({
      devices: devices,
    });
  }

  toggleMonitoring(deviceId) {
    this.sendToBackend({
      request: "toggle_port_state",
      deviceId: deviceId,
      state: !this.state.devices[deviceId].monitor,
    });
  }

  toggleMonitoringAllPorts() {
    let isMonitoringAllPorts = !this.state.isMonitoringAllPorts;
    const devices = this.state.devices;

    for (let deviceId in devices)
      devices[deviceId].monitor = isMonitoringAllPorts;

    const minimizedDashBoards = this.state.minimizedDashBoards;

    for (let deviceId in minimizedDashBoards) {
      minimizedDashBoards[deviceId].monitor = isMonitoringAllPorts;
    }

    this.sendToBackend({
      request: "set_ports_state",
      value: isMonitoringAllPorts,
    });
  }

  clearHistory(deviceId) {
    const devicesInfo = this.state.devices;
    devicesInfo[deviceId].history = Array();

    this.setState({
      devices: devicesInfo,
    });
  }

  deleteCommandFromSequence(index) {
    const newSequence = this.state.newSequence;
    const filteredSequence = Array();

    for (let i = 0; i < newSequence.length; i++) {
      if (i !== index) {
        filteredSequence.push(newSequence[i]);
      }
    }

    this.setState({
      newSequence: filteredSequence,
    });
  }

  setPauseValue(index, value) {
    const sequence = this.state.newSequence;

    sequence[index] = {
      pause: value,
    };

    this.setState({
      newSequence: sequence,
    });
  }

  addPauseToSequence(time) {
    const newSequence = this.state.newSequence;

    newSequence.push({
      pause: time,
    });

    this.setState({
      newSequence: newSequence,
    });
  }

  stopBlinking(deviceId) {
    const devices = this.state.devices;
    devices[deviceId].blinking = false;

    this.setState({
      devices: devices,
    });
  }

  render() {
    if (this.state.devices == {}) {
      return <div className={"box"}>No devices data connected</div>;
    } else {
      let historyNonEmpty = false;

      for (let deviceId in this.state.devices) {
        if (this.state.devices[deviceId].history.length > 0) {
          historyNonEmpty = true;
          break;
        }
      }

      return (
        <div>
          <TopBar
            historyNonEmpty={historyNonEmpty}
            isAgentConnected={this.state.isAgentConnected}
            isRecordingSequence={this.state.isRecordingSequence}
            isRunningSequence={this.state.isRunningSequence}
            isMonitoringAllPorts={this.state.isMonitoringAllPorts}
            isNewSequenceNameDialogActive={
              this.state.isNewSequenceNameDialogActive
            }
            isBackendAvailable={this.state.isBackendAvailable}
            endlessLoop={this.state.endlessLoop}
            sequenceToRun={this.state.sequenceToRun}
            stationName={stationName}
            sequences={this.state.sequences}
            minimizedDashBoards={this.state.minimizedDashBoards}
            setSequenceToRun={this.setSequenceToRun}
            toggleRunningSequence={this.toggleRunningSequence}
            toggleRecordingSequence={this.toggleRecordingSequence}
            clearHistoryAll={this.clearHistoryAll}
            toggleMonitoringAllPorts={this.toggleMonitoringAllPorts}
            maximizeDashBoard={this.maximizeDashBoard}
            setNewSequenceName={this.setNewSequenceName}
            discardNewSequence={this.discardNewSequence}
            toggleEndlessLoop={this.toggleEndlessLoop}
            toggleEditorMode={this.toggleEditorMode}
          />
          <DashBoards
            endlessLoop={this.state.endlessLoop}
            isBackendAvailable={this.state.isBackendAvailable}
            isRecordingSequence={this.state.isRecordingSequence}
            isRunningSequence={this.state.isRunningSequence}
            devices={this.state.devices}
            maximizedDashBoards={this.state.maximizedDashBoards}
            newSequence={this.state.newSequence}
            newSequenceName={this.state.newSequenceName}
            schemes={this.state.schemes}
            sequenceToRun={this.state.sequenceToRun}
            sequenceName={this.state.sequenceToRun}
            sequences={this.state.sequences}
            portsConfig={this.state.portsConfig}
            addPauseToSequence={this.addPauseToSequence}
            clearHistory={this.clearHistory}
            closeRunningSequence={this.closeRunningSequence}
            deleteCommandFromSequence={this.deleteCommandFromSequence}
            discardNewSequence={this.discardNewSequence}
            minimizeDashBoard={this.minimizeDashBoard}
            postPortConfig={this.postPortConfig}
            setPauseValue={this.setPauseValue}
            setSequenceToRun={this.setSequenceToRun}
            saveNewSequence={this.saveNewSequence}
            sendToBackend={this.sendToBackend}
            stopBlinking={this.stopBlinking}
            toggleMonitoring={this.toggleMonitoring}
            toggleRunningSequence={this.toggleRunningSequence}
            getStationConfig={this.getStationConfig}
          />
          <MinimizedPortDashboards
            minimizedDashBoards={this.state.minimizedDashBoards}
            maximizeDashBoard={this.maximizeDashBoard}
          />
        </div>
      );
    }
  }
}

export default StationMonitor;
