import React from "react";
import ReactDOM from "react-dom";
import StationMonitor from "./station-monitor";
require("./app.scss");
import { library } from "@fortawesome/fontawesome-svg-core";
import { faStroopwafel } from "@fortawesome/free-solid-svg-icons";
library.add(faStroopwafel);

ReactDOM.render(
  <StationMonitor stationId={stationId} />,
  document.getElementById("station-monitor")
);
