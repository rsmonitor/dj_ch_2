// todo: Show connected but not configured devices in Main DashBoard
import React from "react";
import NewSequenceTable from "./toolbar/new-sequence-table";
import SequenceRunner from "./toolbar/sequence-runner";
import PortDashBoard from "./port-dashboard/port-dash-board";

function DashBoards(props) {
  const elementsArray = Array();

  if (props.isRecordingSequence) {
    elementsArray.push(
      <NewSequenceTable
        key={"sequence-editor"}
        sequence={props.newSequence}
        newSequenceName={props.newSequenceName}
        deleteCommandFromSequence={props.deleteCommandFromSequence}
        saveNewSequence={props.saveNewSequence}
        discardNewSequence={props.discardNewSequence}
        addPauseToSequence={props.addPauseToSequence}
        setPauseValue={props.setPauseValue}
      />
    );
  } else if (props.sequenceToRun) {
    elementsArray.push(
      <SequenceRunner
        key={"sequence-runner"}
        sequenceToRun={props.sequences[props.sequenceToRun]}
        sequenceName={props.sequenceToRun}
        isRunningSequence={props.isRunningSequence}
        closeRunningSequence={props.closeRunningSequence}
        sendToBackend={props.sendToBackend}
        devices={props.devices}
        isBackendAvailable={props.isBackendAvailable}
        toggleRunningSequence={props.toggleRunningSequence}
        endlessLoop={props.endlessLoop}
        setSequenceToRun={props.setSequenceToRun}
      />
    );
  }

  const devices = props.devices;

  if (props.portsConfig) {
    const ports = props.portsConfig;

    for (let i = 0; i < ports.length; i++) {
      let portConfig = ports[i];
      let deviceId = portConfig.port_os_id;

      if (
        !props.maximizedDashBoards.hasOwnProperty(deviceId) ||
        !props.portsConfig
      ) {
        continue;
      }

      elementsArray.push(
        <PortDashBoard
          key={deviceId}
          isRecordingSequence={props.isRecordingSequence}
          isRunningSequence={props.isRunningSequence}
          deviceData={devices[deviceId]}
          deviceId={deviceId}
          portConfig={portConfig}
          sequenceToRun={props.sequenceToRun}
          schemes={props.schemes}
          clearHistory={props.clearHistory}
          minimizeDashBoard={props.minimizeDashBoard}
          sendToBackend={props.sendToBackend}
          stopBlinking={props.stopBlinking}
          togglePortSettingsDialog={props.togglePortSettingsDialog}
          toggleMonitoring={props.toggleMonitoring}
          getStationConfig={props.getStationConfig}
        />
      );
    }
  }

  const divider = elementsArray.length ? (
    <div className={"is-divider is-narrow"} />
  ) : undefined;

  return (
    <div className={"all-dashboards"} id={"dashboards"}>
      <div className={"columns is-multiline"}>{elementsArray}</div>
      {divider}
    </div>
  );
}

export default DashBoards;
