import ReconnectingWebSocket from "reconnecting-websocket";

function copyToClipboard (event, str) {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    const selected = document.getSelection().rangeCount > 0 ? document.getSelection().getRangeAt(0) : false;                                    // Mark as false to know no selection existed before
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);

    if (selected) {
        document.getSelection().removeAllRanges();
        document.getSelection().addRange(selected);
    }
    event.preventDefault();
}

function errorHandler(data) {
    console.log(data);
}

function logger (name, msg){
    console.log(`\n===============\n${name}\n`);
    console.log(msg);
    console.log('\n===============\n\n\n');
}

function createWebSocket(url, onMessageCallback, setConnectionStatus) {
    const backendSocket = new ReconnectingWebSocket(url, []);

    backendSocket.onmessage = (message) => {
        onMessageCallback(message);
    };

    backendSocket.onopen = () => {
        setConnectionStatus(true);

        backendSocket.send(
            JSON.stringify({
                'request': 'get_ports_monitor_status',
                'sender': 'gui'
            })
        );

        backendSocket.send(
            JSON.stringify({
                'request': 'get_attached_com_ports',
                'sender': 'gui'
            })
        );
    };

    backendSocket.onclose = () => {
        setConnectionStatus(false);
    };

    return backendSocket;
}

function isEmpty (obj){

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // If it isn't an object at this point
    // it is empty, but it can't be anything *but* empty
    // Is it empty?  Depends on your application.
    if (typeof obj !== "object") return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (let key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

export { logger, isEmpty, createWebSocket, errorHandler, copyToClipboard };