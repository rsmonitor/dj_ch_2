function delayExecute(wrapped) {
  return function () {
    setTimeout(() => wrapped.apply(this, arguments), 1);
  };
}

function fetchFromUrl(url, successCallback, errorCallback) {
  fetch(url, {
    method: "GET",
    credentials: "same-origin",
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        //todo: inform the GUI in some way about the ERROR
        errorCallback(response);
      }
    })
    .then((data) => {
      successCallback(data);
    });
}

function postToUrl(url, data, successCallback, errorCallback) {
  const csrfRegEx = "csrftoken=([0-9a-zA-Z]*){64}";
  const csrftoken = document.cookie
    .match(csrfRegEx)[0]
    .replace("csrftoken=", "");

  fetch(url, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "X-CSRFToken": csrftoken,
    },
    credentials: "same-origin",
    redirect: "follow",
    cache: "no-cache",
    mode: "cors",
    body: JSON.stringify(data),
  })
    .then((response) => {
      return response.json();
    })
    .then((result) => {
      if (result.status === 200) {
        successCallback(result);
      } else {
        errorCallback(result);
      }
    })
    .catch((error) => {
      errorCallback(JSON.stringify(error));
    });
}

export { fetchFromUrl, postToUrl };
