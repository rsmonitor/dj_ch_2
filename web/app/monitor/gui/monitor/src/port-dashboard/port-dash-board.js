import React from "react";
import DataMapper from "./data-mapper";
import CommandsButtons from "./commands-buttons";
import Header from "./header/header";

class PortDashBoard extends React.Component {
  constructor(props) {
    super(props);

    this.deviceId = props.deviceId;
    const portConfig = props.portConfig;
    const listsOfCommands = portConfig.lists_of_commands;
    const namesOfListsOfCommands = Array();
    const commandsSets = Object();

    for (let i = 0; i < listsOfCommands.length; i++) {
      let name = listsOfCommands[i].name;
      namesOfListsOfCommands.push(name);
      commandsSets[name] = listsOfCommands[i].commands;
    }

    let activeCommandsSet = namesOfListsOfCommands[0];
    const deviceType = this.deviceId.split("_")[0];

    //Pick a commands list with the name the most closer to the DeviceName
    for (let commandsSet in commandsSets) {
      if (commandsSet.startsWith(deviceType)) {
        activeCommandsSet = commandsSet;
        break;
      }
    }

    this.state = {
      portConfig: portConfig,
      commandsSets: commandsSets,
      namesOfListsOfCommands: namesOfListsOfCommands,
      activeCommandsSet: activeCommandsSet,
      enabled: props.deviceData.monitor,
      settingsDialogOpen: false,
      isRecordingSequence: props.isRecordingSequence,
      history: props.deviceData.history ? props.deviceData.history : Array(),
      monitor: props.deviceData.monitor,
      blinking: props.deviceData.blinking,
    };
    this.sendToBackend = this.sendToBackend.bind(this);
    this.setActiveCommandsSet = this.setActiveCommandsSet.bind(this);
    this.toggleMonitoring = this.toggleMonitoring.bind(this);
  }

  sendToBackend(cmd) {
    this.props.sendToBackend({
      request: "send_command_to_device",
      command: cmd,
      device_id: this.props.deviceId,
    });
  }

  componentWillReceiveProps(nextProps) {
    let monitor = false;
    let history = Array();
    let blinking = false;

    if (nextProps.deviceData) {
      monitor = nextProps.deviceData.monitor;
      history = nextProps.deviceData.history;
      blinking = nextProps.deviceData.blinking;
    }

    this.setState({
      enabled: monitor,
      history: history,
      blinking: blinking,
      portConfig: nextProps.portConfig,
      isRecordingSequence: nextProps.isRecordingSequence,
    });
  }

  toggleMonitoring() {
    const newState = !this.state.enabled;

    this.props.sendToBackend({
      request: "toggle_port_state",
      state: newState,
      device_id: this.deviceId,
    });
  }

  setActiveCommandsSet(activeCommandsSet) {
    console.log(activeCommandsSet);
    this.setState({
      activeCommandsSet: activeCommandsSet,
    });
    event.preventDefault();
  }

  render() {
    if (!Object.keys(this.state.commandsSets)) {
      return <div>"Waiting for a list of commands..."</div>;
    } else {
      const history = this.state.history;
      const historyCopy = this.state.history
        ? JSON.parse(JSON.stringify(history))
        : Array();
      return (
        <div className={"dashboard"}>
          <div
            className={"column is-narrow"}
            id={`port-dash-board-${this.props.deviceId}`}
          >
            <div className={"box"}>
              <Header
                schemes={this.props.schemes}
                portConfig={this.state.portConfig}
                deviceId={this.deviceId}
                history={this.state.history}
                enabled={this.state.enabled}
                namesOfListsOfCommands={this.state.namesOfListsOfCommands}
                activeCommandsSet={this.state.activeCommandsSet}
                getStationConfig={this.props.getStationConfig}
                minimizeDashBoard={this.props.minimizeDashBoard}
                toggleMonitoring={this.toggleMonitoring}
                setActiveCommandsSet={this.setActiveCommandsSet}
                clearHistory={this.props.clearHistory}
                togglePortSettingsDialog={this.props.togglePortSettingsDialog}
              />
              <div className={"columns"}>
                <CommandsButtons
                  sendToBackend={this.sendToBackend}
                  commandsList={
                    this.state.commandsSets[this.state.activeCommandsSet]
                  }
                  enabled={this.state.enabled}
                />
                <DataMapper
                  blinking={this.props.blinking}
                  stopBlinking={() => this.props.stopBlinking(this.deviceId)}
                  history={historyCopy}
                  isRecordingSequence={this.state.isRecordingSequence}
                  isRunningSequence={this.props.isRunningSequence}
                />
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

export default PortDashBoard;
