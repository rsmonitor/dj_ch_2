import React from "react";
import HistoryItem from "./history-item";
const _ = require("lodash");

class DataMapper extends React.Component {
  constructor(props) {
    super(props);
    this.defaultBgColor = { r: 0, g: 0, b: 0 };
    this.timer = 0;
    this.paddingStep = 0;
    this.blinkPause = 50;
    this.blinkSteps = 10;
    this.step = 0;
    this.blinkRedStep = 8;
    this.blinkGreeStep = 1;
    this.blinkBlueStep = 1;
    this.defaultReceivedStyle =
      "has-text-danger has-text-weight-light is-size-7 tooltip is-tooltip-warning";
    this.underMouseReceivedStyle =
      "has-text-danger has-text-weight-bold is-size-7 tooltip is-tooltip-warning";
    this.defaultSentStyle =
      "has-text-info has-text-weight-light is-size-7 tooltip is-tooltip-warning";
    this.underMouseSentStyle =
      "has-text-info has-text-weight-bold is-size-7 tooltip is-tooltip-warning";

    this.state = {
      padding: 0,
      history: props.history,
      bgColor: this.defaultBgColor,
      blinking: props.blinking,
      underMouse: -1,
    };

    this.blink = this.blink.bind(this);
  }

  blink() {
    // return;
    if (this.step >= this.blinkSteps) {
      clearInterval(this.timer);
      this.step = 0;

      this.setState({
        padding: 0,
        bgColor: this.defaultBgColor,
      });

      this.props.stopBlinking();
    } else {
      const newBgColor = {
        r: this.state.bgColor.r + this.blinkRedStep,
        g: this.state.bgColor.g + this.blinkGreeStep,
        b: this.state.bgColor.b + this.blinkBlueStep,
      };

      this.setState({
        padding: this.state.padding + this.paddingStep,
        bgColor: newBgColor,
      });
      this.step++;
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(nextProps.history, this.props.history)) {
      this.setState({
        history: nextProps.history,
      });
    }

    if (nextProps.blinking) {
      clearInterval(this.timer);
      this.timer = 0;
      this.timer = setInterval(() => {
        this.blink(0);
      }, this.blinkPause);
    }
  }

  render() {
    const history = this.state.history;

    if (this.props.isRecordingSequence) {
      return <div />;
    } else if (history === undefined || !history.length) {
      return (
        <div className={"column"}>
          <div className={"box"}>
            <i>{"Empty history"}</i>
          </div>
        </div>
      );
    } else {
      const historyItems = Array();
      let defaultStyle = undefined;
      let underMouseStyle = undefined;

      for (let i = 0; i < history.length; i++) {
        const type = history[i]["type"];

        switch (type) {
          case "sent":
            defaultStyle = this.defaultSentStyle;
            underMouseStyle = this.underMouseSentStyle;
            break;
          case "received":
            defaultStyle = this.defaultReceivedStyle;
            underMouseStyle = this.underMouseReceivedStyle;
            break;
        }

        historyItems[i] = (
          <HistoryItem
            key={i}
            item={history[i]}
            defaultStyle={defaultStyle}
            underMouseStyle={underMouseStyle}
          />
        );
      }

      const r = this.state.bgColor.r;
      const g = this.state.bgColor.g;
      const b = this.state.bgColor.b;

      return (
        <div className={"column"}>
          <div className={"box"}>
            <div
              className={"datamapper"}
              style={{
                paddingTop: this.state.padding + 1 + "px",
                backgroundColor: `rgb(${r}, ${g}, ${b})`,
              }}
            >
              <ul style={{ listStyle: "none" }}>{historyItems}</ul>
              {/*<ul>{historyItems}</ul>*/}
            </div>
          </div>
        </div>
      );
    }
  }
}

export default DataMapper;
