import React from "react";
import { copyToClipboard } from "../utils";

class CommandButton extends React.Component {
  constructor(props) {
    super(props);

    this.defaultStyle = "button is-rounded is-info is-active is-small";
    this.onFocusStyle = "button is-rounded is-danger is-active is-small";
    this.underMouseStyle = "button is-rounded is-link is-small";

    this.state = {
      isOnFocus: false,
      isUnderMouse: undefined,
      isEnabled: props.isEnabled,
    };
    this.onBlur = this.onBlur.bind(this);
    this.onMouseOver = this.onMouseOver.bind(this);
    this.onMouseOut = this.onMouseOut.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      isEnabled: nextProps.isEnabled,
    });
  }

  handleClick() {
    this.setState({
      isOnFocus: true,
    });
    this.props.sendToBackend(this.props.command);
  }

  onBlur() {
    this.setState({
      isOnFocus: false,
    });
  }

  onMouseOver() {
    this.setState({
      isUnderMouse: true,
    });
  }

  onMouseOut() {
    this.setState({
      isUnderMouse: false,
    });
  }

  render() {
    let style = this.defaultStyle;

    if (this.state.isEnabled) {
      if (this.state.isUnderMouse) {
        style = this.underMouseStyle;
      }

      if (this.state.isOnFocus) {
        style = this.onFocusStyle;
      }
    }

    return (
      <div>
        <button
          className={style + " tooltip is-tooltip-right"}
          data-tooltip={this.props.cmdName}
          onClick={this.handleClick}
          disabled={!this.props.isEnabled}
          onBlur={this.onBlur}
          onMouseOver={this.onMouseOver}
          onMouseOut={this.onMouseOut}
          onContextMenu={(event) => copyToClipboard(event, this.props.command)}
        >
          {this.props.command}
        </button>
      </div>
    );
  }
}

export default CommandButton;
