import React from "react";

class CustomCommand extends React.Component {
  constructor(props) {
    super(props);

    this.defaultStyle = "button is-rounded is-info is-active is-small";
    this.onFocusStyle = "button is-rounded is-danger is-active is-small";
    this.underMouseStyle = "button is-rounded is-link is-small";

    this.state = {
      command: undefined,
      isUnderMouse: false,
      isOnFocus: false,
      isEnabled: props.isEnabled,
    };

    this.handleClick = this.handleClick.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onMouseOver = this.onMouseOver.bind(this);
    this.onMouseOut = this.onMouseOut.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      isEnabled: nextProps.isEnabled,
    });
  }

  onChange(newValue) {
    this.setState({
      command: newValue,
    });
  }

  handleClick() {
    this.setState({
      isOnFocus: true,
    });
    this.props.sendToBackend(this.state.command);
  }

  onMouseOver() {
    this.setState({
      isUnderMouse: true,
    });
  }

  onMouseOut() {
    this.setState({
      isUnderMouse: false,
    });
  }

  onBlur() {
    this.setState({
      isOnFocus: false,
    });
  }

  handleKeyPress(event) {
    switch (event.keyCode) {
      case 27:
        this.onBlur();
        break;
      case 13:
        this.handleClick();
        break;
      default:
        break;
    }
  }

  render() {
    const placeHolder = this.state.command ? undefined : "Custom command";
    let buttonStyle = this.defaultStyle;

    if (this.state.isEnabled) {
      if (this.state.isUnderMouse) {
        buttonStyle = this.underMouseStyle;
      }

      if (this.state.isOnFocus) {
        buttonStyle = this.onFocusStyle;
      }
    }

    return (
      <div onKeyDown={this.handleKeyPress}>
        <input
          className={"input is-rounded"}
          style={{ width: 200 + "px" }}
          onChange={(event) => this.onChange(event.target.value)}
          placeholder={placeHolder}
          disabled={!this.state.isEnabled}
        />
        <br />
        <button
          className={buttonStyle + " tooltip"}
          data-tooltip={"Custom command"}
          onClick={this.handleClick}
          disabled={!this.state.isEnabled || !this.state.command}
          onBlur={this.onBlur}
          onMouseOver={this.onMouseOver}
          onMouseOut={this.onMouseOut}
        >
          {this.state.command}
        </button>
      </div>
    );
  }
}

export default CustomCommand;
