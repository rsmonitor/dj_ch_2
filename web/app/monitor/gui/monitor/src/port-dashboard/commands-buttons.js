import React from "react";
import CustomCommand from "./custom-command";
import CommandButton from "./command-button";

class CommandsButtons extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      commandsList: props.commandsList,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.commandsList !== this.state.commandsList) {
      this.setState({
        commandsList: nextProps.commandsList,
      });
    }
  }

  handleClick(command) {
    this.props.sendToBackend(command);
  }

  render() {
    const commandsList = this.state.commandsList;

    if (!commandsList || !Object.keys(commandsList).length) {
      //todo: <a href={"/operator_home/"}>
      return (
        <div className={"column is-narrow"}>
          <p>No configured commands...</p>
          <p>
            <a href={"/operator_home/"}>
              Try to configure commands on your home page
            </a>
          </p>
        </div>
      );
    } else {
      const commandsButtons = Array();

      for (let i = 0; i < commandsList.length; i++) {
        let cmdName = commandsList[i].name;
        let command = commandsList[i].cmd;

        commandsButtons.push(
          <CommandButton
            key={i}
            cmdName={cmdName}
            command={command}
            onClick={this.handleClick}
            isEnabled={this.props.enabled}
            sendToBackend={this.props.sendToBackend}
          />
        );
      }

      return (
        <div className={"column is-narrow"}>
          <CustomCommand
            isEnabled={this.props.enabled}
            sendToBackend={this.props.sendToBackend}
          />
          {commandsButtons}
        </div>
      );
    }
  }
}

export default CommandsButtons;
