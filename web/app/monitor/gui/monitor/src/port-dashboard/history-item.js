import React from "react";

class HistoryItem extends React.Component {
  constructor(props) {
    super(props);
    this.type = props.item.type;
    this.item = props.item;

    this.state = {
      style: props.defaultStyle,
    };

    this.onMouseOver = this.onMouseOver.bind(this);
    this.onMouseOut = this.onMouseOut.bind(this);
  }

  onMouseOver() {
    this.setState({
      style: this.props.underMouseStyle,
    });
  }

  onMouseOut() {
    this.setState({
      style: this.props.defaultStyle,
    });
  }

  render() {
    return (
      <div>
        <li
          className={this.state.style}
          data-tooltip={`${this.type.toLocaleUpperCase()}: ${
            this.item.timeStamp
          }`}
          onMouseOver={this.onMouseOver}
          onMouseOut={this.onMouseOut}
        >
          <div dangerouslySetInnerHTML={{ __html: this.item.frame }} />
          <div className={"divider"} />
        </li>
      </div>
    );
  }
}

export default HistoryItem;
