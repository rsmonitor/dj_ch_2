import React from "react";

function InfoBar(props) {
  let icon = props.status ? "checkmark-circle" : "close-circle";

  return (
    <div className={"control"}>
      <span className={"icon"}>
        <ion-icon name={icon} />
      </span>
    </div>
  );
}

export default InfoBar;
