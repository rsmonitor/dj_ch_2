import React from "react";

function ClearHistory(props) {
  return (
    <div className={"control tooltip"} data-tooltip="Clear history">
      <button
        className={"button is-small is-rounded is-dark"}
        onClick={() => props.clearHistory(props.deviceId)}
        disabled={!props.history.length}
      >
        <span className={"icon"}>
          <ion-icon name={"trash"} />
        </span>
      </button>
    </div>
  );
}

export default ClearHistory;
