import React from "react";

function PickCommandsSet(props) {
  function handleRightClick(e) {
    console.log(e.target.value);
    e.preventDefault();
  }

  const commandsSet = Array();

  for (let i = 0; i < props.namesOfListsOfCommands.length; i++) {
    let setName = props.namesOfListsOfCommands[i];
    commandsSet.push(
      <option key={i} value={setName} onContextMenu={handleRightClick}>
        {setName}
      </option>
    );
  }

  return (
    <div className={"control tooltip"} data-tooltip={"Pick a set of commands"}>
      <div className={"select is-small is-rounded"}>
        <select
          onChange={(event) => props.setActiveCommandsSet(event.target.value)}
          value={props.activeCommandsSet}
          onContextMenu={handleRightClick}
        >
          {commandsSet}
        </select>
      </div>
    </div>
  );
}

export default PickCommandsSet;
