import React from "react";
import _ from "lodash";
import { postToUrl } from "../../api-fetch/api-fetch";

class PortSettingsDialog extends React.Component {
  constructor(props) {
    super(props);
    this.portId = props.portId;
    this.portOsId = props.portOsId;

    const initialConfigCopy = JSON.parse(JSON.stringify(props.portConfig));
    delete initialConfigCopy["lists_of_commands"];

    const portConfigCopy = JSON.parse(JSON.stringify(props.portConfig));
    delete portConfigCopy["lists_of_commands"];

    this.state = {
      modal: props.isPortSettingsDialogActive,
      initialConfig: initialConfigCopy,
      portConfig: portConfigCopy,
      schema: props.schema,
      submitEnabled: false,
    };

    this.onChange = this.onChange.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this._createOptionsField = this._createOptionsField.bind(this);
    this._createBooleanField = this._createBooleanField.bind(this);
    this._createTextNumField = this._createTextNumField.bind(this);
    this._createIntegerField = this._createIntegerField.bind(this);
    this.toggleState = this.toggleState.bind(this);
    this.saveNewPortSettings = this.saveNewPortSettings.bind(this);
    this.convertPlainObject = this.convertPlainObject.bind(this);
  }

  onChange(name, event) {
    let newPortConfig = this.state.portConfig;
    newPortConfig[name] = event.target.value;

    this.setState({
      portConfig: newPortConfig,
      submitEnabled: !_.isEqual(
        this.convertPlainObject(newPortConfig),
        this.state.initialConfig
      ),
    });
  }

  toggleState(name) {
    const portConfig = this.state.portConfig;
    portConfig[name] = !portConfig[name];

    this.setState({
      portConfig: portConfig,
      submitEnabled: !_.isEqual(
        this.convertPlainObject(portConfig),
        this.state.initialConfig
      ),
    });
  }

  convertPlainObject(data) {
    const result = {};

    for (let field in data) {
      let type = this.state.schema.properties[field].type;

      switch (type) {
        case "integer":
          result[field] = parseInt(data[field]);
          break;
        case "boolean":
          result[field] = data[field];
          break;
        case "number":
          result[field] = parseFloat(data[field]);
          break;
        case "string":
          if (typeof data[field] !== "string") {
            result[field] = toString(data[field]);
          } else {
            result[field] = data[field];
          }
          break;
      }
    }
    return result;
  }

  saveNewPortSettings() {
    if (!this.state.submitEnabled) {
      return;
    }
    let portConfig = this.state.portConfig;
    portConfig.pk = this.state.portId;
    portConfig["port_os_id"] = this.state.portOsId;
    delete portConfig.lists_of_commands;
    portConfig = this.convertPlainObject(portConfig);

    postToUrl(
      "/monitor/api/ports/",
      {
        new_port_config: portConfig,
      },
      (res) => {
        alert(`New settings are applied\n${JSON.stringify(res)}`);
        this.props.getStationConfig();
      },
      (e) => {
        alert(`Failed to save the new settings:\n${e}`);
      }
    );

    this.props.togglePortSettingsDialog();
  }

  componentWillReceiveProps(nextProps) {
    const initialConfigCopy = JSON.parse(JSON.stringify(nextProps.portConfig));
    delete initialConfigCopy["lists_of_commands"];
    const portConfigCopy = JSON.parse(JSON.stringify(nextProps.portConfig));
    delete portConfigCopy["lists_of_commands"];

    this.setState({
      initialConfig: initialConfigCopy,
      portConfig: portConfigCopy,
      modal: nextProps.isPortSettingsDialogActive,
      schema: nextProps.schema,
      submitEnabled: false,
    });
  }

  handleKeyPress(event) {
    // console.log(event);
    switch (event.keyCode) {
      case 27:
        this.props.togglePortSettingsDialog();
        break;
      case 13:
        this.saveNewPortSettings();
        break;
      default:
        break;
    }
  }

  _createOptionsField(name, title, enumerated) {
    const options = Array();

    for (let i in enumerated) {
      let record = enumerated[i];

      options[i] = (
        <option key={i} value={record[0]}>
          {record[1]}
        </option>
      );
    }

    return (
      <div key={name} className={"field is-horizontal"}>
        <div className={"field-label"}>
          <label className={"label"}>{title}</label>
        </div>

        <div className={"field-body"}>
          <div className={"field is-narrow"}>
            <div className={"control"}>
              <div className={"select is-rounded"}>
                <select
                  onChange={(event) => this.onChange(name, event)}
                  value={this.state.portConfig[name]}
                  style={{ fontWeight: "bold" }}
                >
                  {options}
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  _createBooleanField(name, title) {
    const elementId = `${this.state.portConfig.port_os_id}_${name}`;

    return (
      <div key={name} className={"field is-horizontal"}>
        <div className={"field-label"}>
          <label className={"label"}>{title}</label>
        </div>

        <div className={"field-body"}>
          <div className={"field is-narrow"}>
            <div className={"control"}>
              <input
                id={elementId}
                type={"checkbox"}
                className={"switch is-rounded outlined"}
                checked={this.state.portConfig[name]}
                onChange={() => this.toggleState(name)}
              />
              <label htmlFor={elementId} className={"field-label"} />
            </div>
          </div>
        </div>
      </div>
    );
  }

  _createTextNumField(name, title) {
    return (
      <div key={name} className={"field is-horizontal"}>
        <div className={"field-label"}>
          <label className={"label"}>{title}</label>
        </div>
        <div className={"field-body"}>
          <div className={"field"}>
            <input
              className={"input"}
              type={"text"}
              value={this.state.portConfig[name]}
              onChange={(event) => this.onChange(name, event)}
              style={{
                fontWeight: "bold",
                width: 100 + "px",
              }}
            />
          </div>
        </div>
      </div>
    );
  }

  _createIntegerField(name, title) {
    return (
      <div key={name} className={"field is-horizontal"}>
        <div className={"field-label"}>
          <label className={"label"}>{title}</label>
        </div>
        <div className={"field-body"}>
          <div className={"field"}>
            <input
              className={"input"}
              type={"number"}
              value={this.state.portConfig[name]}
              onChange={(event) => this.onChange(name, event)}
              style={{
                fontWeight: "bold",
                width: 100 + "px",
              }}
            />
          </div>
        </div>
      </div>
    );
  }

  _createField(name, ...params) {
    const title = params[0].title;
    const type = params[0].type;
    const enumerated = params[0].enum;
    const x_nullable = params[0]["x-nullable"];

    if (enumerated) {
      return this._createOptionsField(name, title, enumerated);
    } else if (type === "boolean") {
      return this._createBooleanField(name, title);
    } else if (type === "string" || type === "text" || type === "number") {
      return this._createTextNumField(name, title);
    } else if (type === "integer") {
      return this._createIntegerField(name, title);
    }
  }

  render() {
    if (!this.state.schema || !this.state.portConfig || !this.state.modal) {
      return <div className={"modal"} />;
    } else {
      const schema = this.state.schema.properties;
      const fieldsToOmit = ["lists_of_commands", "port_os_id", "pk"];
      const formFields = Array();

      for (let name in schema) {
        if (fieldsToOmit.includes(name)) {
          continue;
        }
        formFields.push(this._createField(name, schema[name]));
      }

      return (
        <div
          ref={(c) => (this._input = c)}
          className={"modal is-active"}
          onKeyDown={this.handleKeyPress}
        >
          <div className={"modal-background"} />
          <div className={"modal-card"}>
            <header className={"modal-card-head"}>
              <p className={"modal-card-title"}>
                Update <strong>{this.portOsId}</strong>'s settings
              </p>
            </header>

            <section className={"modal-card-body"}>{formFields}</section>

            <footer className={"modal-card-foot"}>
              <button
                className="button is-info"
                onClick={this.saveNewPortSettings}
                disabled={!this.state.submitEnabled}
              >
                {"OK"}
              </button>
              <button
                className={"button is-danger"}
                onClick={this.props.togglePortSettingsDialog}
              >
                {"Cancel"}
              </button>
            </footer>
          </div>
        </div>
      );
    }
  }
}

export default PortSettingsDialog;
