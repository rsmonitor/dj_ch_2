import React from "react";

function TogglePortMonitoring(props) {
  const icon = props.monitoringState ? "eye-off" : "eye";
  const dataTooltip = props.monitoringState
    ? "Release the port"
    : "Open the port";

  return (
    <div className={"control"}>
      <button
        className={"button is-small is-rounded is-danger tooltip"}
        data-tooltip={dataTooltip}
        onClick={() => props.toggleMonitoringState(props.deviceId)}
      >
        <span className={"icon"}>
          <ion-icon name={icon} />
        </span>
      </button>
    </div>
  );
}

export default TogglePortMonitoring;
