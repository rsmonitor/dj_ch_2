import React from "react";
import Popup from "reactjs-popup";

class SettingsPopup extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      portConfig: props.portConfig,
    };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({
      open: true,
    });
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      portConfig: newProps.portConfig,
    });
  }

  closeModal() {
    this.setState({
      open: false,
    });
  }

  render() {
    return (
      <div>
        <button
          className="button is-small is-rounded is-primary"
          onClick={this.openModal}
        >
          Port settings
        </button>
        <Popup
          open={this.state.open}
          closeOnDocumentClick
          onClose={this.closeModal}
        >
          <div>
            <a className="close" onClick={this.closeModal}>
              &times;
            </a>
            <strong>{JSON.stringify(this.state.portConfig)}</strong>
            <div>Hello, world!</div>
          </div>
        </Popup>
      </div>
    );
  }
}

export default SettingsPopup;
