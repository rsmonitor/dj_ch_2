import React from "react";
import InfoBar from "./info-bar";
import TogglePortMonitoring from "./toggle-port-monitoring";
import PickCommandsSet from "./pick-commands-set";
import ClearHistory from "./clear-history";
import PortSettingsToggler from "./port-settings-toggler";
import PortSettingsDialog from "./port-settings-dialog";

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isPortSettingsDialogActive: false,
      portId: props.portConfig.pk,
      deviceId: props.deviceId,
      portOsId: props.portConfig.port_os_id,
      listsOfCommands: props.portConfig.lists_of_commands,
    };

    this.togglePortSettingsDialog = this.togglePortSettingsDialog.bind(this);
  }

  togglePortSettingsDialog() {
    this.setState({
      isPortSettingsDialogActive: !this.state.isPortSettingsDialogActive,
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      portId: nextProps.portConfig.pk,
      deviceId: nextProps.deviceId,
      portOsId: nextProps.portConfig.port_os_id,
      listsOfCommands: nextProps.portConfig.lists_of_commands,
    });
  }

  render() {
    return (
      <div className={"tile is-ancestor"}>
        <div className={"tile is-parent"}>
          <article className={"tile is-child notification is-warning"}>
            <div className={"content"}>
              <div className={"subtitle"}>
                <div className={"field is-grouped"}>
                  <div className={"control"}>
                    <a
                      className={"delete"}
                      onClick={() =>
                        this.props.minimizeDashBoard(this.state.deviceId)
                      }
                    />
                  </div>
                  <div className={"control"}>
                    <h5 className={"title is-5"}>{this.state.deviceId}</h5>
                  </div>
                  <InfoBar status={this.props.enabled} />
                  {/*Incoming message blinking indicator*/}
                  <div>
                    <span className={"icon"}>
                      <ion-icon name={"pulse"} />
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <PortSettingsDialog
              isPortSettingsDialogActive={this.state.isPortSettingsDialogActive}
              togglePortSettingsDialog={this.togglePortSettingsDialog}
              portConfig={this.props.portConfig}
              schema={this.props.schemes.Port}
              portOsId={this.state.portOsId}
              portId={this.state.portId}
              getStationConfig={this.props.getStationConfig}
            />
            <div className={"field has-addons"}>
              <ClearHistory
                deviceId={this.state.deviceId}
                clearHistory={this.props.clearHistory}
                history={this.props.history}
              />
              <TogglePortMonitoring
                monitoringState={this.props.enabled}
                toggleMonitoringState={this.props.toggleMonitoring}
                deviceId={this.state.deviceId}
              />
              <PortSettingsToggler
                togglePortSettingsDialog={this.togglePortSettingsDialog}
              />
              <PickCommandsSet
                namesOfListsOfCommands={this.props.namesOfListsOfCommands}
                activeCommandsSet={this.props.activeCommandsSet}
                setActiveCommandsSet={this.props.setActiveCommandsSet}
                disabled={this.props.disabled}
              />
            </div>
          </article>
        </div>
      </div>
    );
  }
}

export default Header;
