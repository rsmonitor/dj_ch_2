import React from "react";

function PortSettingsToggler(props) {
  return (
    <div className={"control tooltip"} data-tooltip="Settings">
      <button
        className={"button is-small is-rounded is-dark"}
        onClick={props.togglePortSettingsDialog}
      >
        <span className={"icon"}>
          <ion-icon name={"settings"} />
        </span>
      </button>
    </div>
  );
}

export default PortSettingsToggler;
