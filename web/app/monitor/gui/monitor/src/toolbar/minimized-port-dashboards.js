import React from "react";

function MinimizedPortDashboards(props) {
  const devices = Array();
  const defaultStyle = "button is-rounded is-dark is-small";

  for (let deviceName in props.minimizedDashBoards) {
    devices.push(
      <button
        key={deviceName}
        className={defaultStyle}
        onClick={() => props.maximizeDashBoard(deviceName)}
      >
        <span className={"icon"}>
          <ion-icon name="pulse" />
        </span>
        <strong>{deviceName}</strong>
      </button>
    );
  }

  return (
    <nav className={"navbar is-fixed-bottom is-black"} id={"bottom-bar"}>
      <div className={"column"}>
        <div className={"field is-grouped"}>{devices}</div>
      </div>
    </nav>
  );
}

export default MinimizedPortDashboards;
