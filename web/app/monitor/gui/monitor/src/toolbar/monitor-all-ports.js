import React from "react";

function MonitorAllPorts(props) {
  let iconName = props.isMonitoringAllPorts ? "eye-off" : "eye";
  const dataTooltip = props.isMonitoringAllPorts
    ? "Release all ports"
    : "Monitor all ports";
  const className = "button is-rounded";

  return (
    <div className={"control"}>
      <button
        className={className + " tooltip is-tooltip-bottom"}
        data-tooltip={dataTooltip}
        onClick={props.toggleMonitoringAllPorts}
        disabled={props.disabled}
      >
        <span className={"icon"}>
          <ion-icon name={iconName} />
        </span>
      </button>
    </div>
  );
}

export default MonitorAllPorts;
