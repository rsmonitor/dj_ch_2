import React from "react";

function BackendStatus(props) {
  const backendIconName = props.isBackendAvailable ? "flash" : "flash-off";

  return (
    <div
      className={"control tooltip is-tooltip-bottom"}
      data-tooltip={"Backend status"}
    >
      <button className={"button is-rounded is-static"}>
        <span className={"icon"}>
          <ion-icon name={backendIconName} />
        </span>
      </button>
    </div>
  );
}

export default BackendStatus;
