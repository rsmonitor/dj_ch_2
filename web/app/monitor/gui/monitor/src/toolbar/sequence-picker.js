import React from "react";

function SequencePicker(props) {
  const sequences = Array();
  sequences.push(
    <option key={""} value={""}>
      {"Run a sequence..."}
    </option>
  );

  for (let sequenceName in props.sequences) {
    sequences.push(
      <option key={sequenceName} value={sequenceName}>
        {sequenceName}
      </option>
    );
  }

  const className = props.hidden
    ? "select is-rounded is-hidden"
    : "select is-rounded";

  return (
    <div className={"control has-icons-left"}>
      <div className="field">
        <div className={className}>
          <select
            onChange={(event) => props.setSequenceToRun(event.target.value)}
            disabled={props.disabled}
            value={props.sequenceToRun}
          >
            {sequences}
          </select>
        </div>
        <div className="icon is-small is-left">
          <ion-icon name="menu" />
        </div>
      </div>
    </div>
  );
}

export default SequencePicker;
