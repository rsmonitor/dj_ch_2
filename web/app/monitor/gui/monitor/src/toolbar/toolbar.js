import React from "react";
import SequencePicker from "../sequences-toolbar/sequence-picker";
import SaveSequenceDialog from "../sequences-toolbar/save-sequence-dialog";
import fetchFromUrl from "../api-fetch/api-fetch";
import RecordSequence from "../sequences-toolbar/record-sequence";

class ToolBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sequences: props.sequences,
      newSequenceName: undefined,
      newSequence: Array(),
      isSaveSequenceDialogActive: false,
      isRecordingNewSequence: props.isRecordingNewSequence,
      isRunningSequence: false,
      sequenceToRun: undefined,
      isSequencePickerDisabled: false,
    };
    this.updateSequences = this.updateSequences.bind(this);
    this.setSequences = this.setSequences.bind(this);
    this.setSequenceToRun = this.setSequenceToRun.bind(this);
    this.setNewSequenceName = this.setNewSequenceName.bind(this);
    this.saveNewSequence = this.saveNewSequence.bind(this);
    this.discardNewSequence = this.discardNewSequence.bind(this);
  }

  componentDidMount() {
    this.updateSequences();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      isRecordingNewSequence: nextProps.isRecordingNewSequence,
    });
  }

  setSequences(sequences) {
    this.setState({
      sequences: sequences,
    });
  }

  updateSequences() {
    fetchFromUrl("/monitor/get_sequences", this.setSequences);
  }

  setSequenceToRun(sequence) {
    this.setState({
      sequenceToRun: sequence,
    });
  }

  setNewSequenceName(newSequenceName) {
    this.setState({
      newSequenceName: newSequenceName,
    });
  }

  saveNewSequence() {
    console.log(
      `SAVING ${this.state.newSequenceName}\n${this.state.newSequence}`
    );
    this.discardNewSequence();
    this.updateSequences();
  }

  discardNewSequence() {
    this.setState({
      newSequence: Array(),
      newSequenceName: undefined,
    });
  }

  render() {
    return (
      <div className={"field is-grouped"}>
        <div className={"field has-addons"}>
          <SaveSequenceDialog
            active={this.state.isSaveSequenceDialogActive}
            newSequenceName={this.state.newSequenceName}
            setSequenceName={this.setNewSequenceName}
            discardNewSequence={this.discardNewSequence}
            saveNewSequence={this.saveNewSequence}
          />
          <SequencePicker
            sequences={this.state.sequences}
            setSequenceToRun={this.setSequenceToRun}
            disabled={this.state.isSequencePickerDisabled}
          />
          <RecordSequence
            isRecordingNewSequence={this.state.isRecordingNewSequence}
            toggleRecording={this.props.toggleRecording}
          />
        </div>
      </div>
    );
  }
}

export default ToolBar;
