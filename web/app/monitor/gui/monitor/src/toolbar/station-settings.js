import React from "react";

function StationSettings(props) {
  return (
    <div className={"control"}>
      <button
        className={"button is-rounded is-primary tooltip is-tooltip-bottom"}
        data-tooltip={"Station settings"}
        disabled={props.disabled}
      >
        <span className={"icon"}>
          <ion-icon name={"settings"} />
        </span>
      </button>
    </div>
  );
}

export default StationSettings;
