import React from "react";

function ToggleSequenceEditor(props) {
  return (
    <div className={"control"}>
      <button
        className="button is-rounded is-black tooltip is-tooltip-bottom"
        data-tooltip={"Edit the sequence"}
        onClick={props.toggleEditorMode}
        disabled={props.disabled}
      >
        <span className={"icon"}>
          <ion-icon name={"create"} />
        </span>
      </button>
    </div>
  );
}

export default ToggleSequenceEditor;
