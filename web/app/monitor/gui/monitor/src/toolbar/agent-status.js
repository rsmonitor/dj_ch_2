import React from "react";

function AgentStatus(props) {
  if (!props.isBackendAvailable) {
    return <div />;
  }

  const agentIconName = props.agentConnected ? "flash" : "flash-off";
  const status = props.agentConnected ? "OK" : "Disconnected";

  return (
    <div
      className={"control tooltip is-tooltip-bottom"}
      data-tooltip={`Agent: ${status}`}
    >
      <button className={"button is-rounded is-static"}>
        <span className={"icon"}>
          <ion-icon name={agentIconName} />
        </span>
      </button>
    </div>
  );
}

export default AgentStatus;
