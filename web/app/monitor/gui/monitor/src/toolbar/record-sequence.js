import React from "react";
import SetSequenceNameDialog from "./set-sequence-name-dialog";

function RecordSequence(props) {
  return (
    <div className={"control"}>
      <button
        className={"button is-rounded is-danger tooltip is-tooltip-bottom"}
        data-tooltip={"Record a new sequence"}
        onClick={props.toggleRecordingSequence}
        disabled={props.disabled}
      >
        <span className={"icon"}>
          <ion-icon name={"recording"} />
        </span>
        {/*<FontAwesomeIcon icon="pencil-alt" />*/}
      </button>
      <SetSequenceNameDialog
        setNewSequenceName={props.setNewSequenceName}
        sequences={props.sequences}
        active={props.isNewSequenceNameDialogActive}
        discardNewSequence={props.discardNewSequence}
      />
    </div>
  );
}

export default RecordSequence;
