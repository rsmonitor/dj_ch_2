import React from "react";

function ToggleEndlessLoop(props) {
  const toolTip = props.endlessLoop ? "Run just once" : "Run forever";
  return (
    <div className={"control"}>
      <div className="field is-grouped">
        <div
          className={"field tooltip is-tooltip-bottom"}
          data-tooltip={toolTip}
        >
          <span className={"icon"}>
            <ion-icon name={"infinite"} />
          </span>

          <input
            type="checkbox"
            className={"switch is-rounded"}
            id={"endless-loop"}
            onChange={props.toggleEndlessLoop}
            disabled={props.disabled}
          />
          <label htmlFor="endless-loop" />
        </div>
      </div>
    </div>
  );
}

export default ToggleEndlessLoop;
