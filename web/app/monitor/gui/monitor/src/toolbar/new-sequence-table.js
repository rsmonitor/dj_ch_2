import React from "react";

function NewSequenceTable(props) {
  const commandsList = Array();
  let command = undefined;
  let deviceCell = undefined;
  let commandCell = undefined;

  for (let i = 0; i < props.sequence.length; i++) {
    command = props.sequence[i];

    if (command.hasOwnProperty("pause")) {
      deviceCell = (
        <td>
          <span className={"icon tooltip"} data-tooltip={"Delay"}>
            <ion-icon name="stopwatch" />
          </span>
        </td>
      );
      commandCell = (
        <td>
          <input
            className={"input"}
            type={"text"}
            placeholder={"time, [s]"}
            value={command.pause}
            onChange={(event) => props.setPauseValue(i, event.target.value)}
          />
        </td>
      );
    } else {
      deviceCell = (
        <td>
          <strong>{command.device}</strong>
        </td>
      );
      commandCell = <td>{command.command}</td>;
    }

    commandsList.push(
      <tr key={i}>
        <td className={"delete-column"}>
          <button
            className={"button is-black is-rounded tooltip"}
            data-tooltip={"Remove from sequence"}
            onClick={() => props.deleteCommandFromSequence(i)}
          >
            <span className={"icon"}>
              <ion-icon name="cut" />
            </span>
          </button>
        </td>
        {deviceCell}
        {commandCell}
      </tr>
    );
  }

  function confirmDiscard() {
    if (confirm("Discard the sequence?")) {
      props.discardNewSequence();
    }
  }

  function addPauseToSequence() {
    props.addPauseToSequence(1.0);
  }

  return (
    <div className={"new-sequence-table"}>
      <div className={"column is-narrow"}>
        <div className={"box"}>
          <div className="tile is-ancestor">
            <div className="tile is-parent">
              <article className="tile is-child notification is-rounded">
                <div className="content">
                  <div className={"subtitle"}>
                    <div className={"field is-grouped"}>
                      <div className={"control"}>
                        <h3 className={"title is-3"}>
                          {props.newSequenceName}
                        </h3>
                      </div>
                      <div className={"control"} />
                      <div className={"field has-addons"}>
                        <div className={"control"}>
                          <button
                            className={
                              "button is-rounded is-dark tooltip is-tooltip-bottom"
                            }
                            data-tooltip={"Save the sequence"}
                            onClick={props.saveNewSequence}
                            disabled={!props.sequence.length}
                          >
                            <span className={"icon"}>
                              <ion-icon name="save" />
                            </span>
                          </button>
                        </div>
                        <div className={"control"}>
                          <button
                            className={
                              "button is-rounded is-success tooltip is-tooltip-bottom"
                            }
                            data-tooltip={"Add delay"}
                            onClick={addPauseToSequence}
                          >
                            <span className={"icon"}>
                              <ion-icon name="stopwatch" />
                            </span>
                          </button>
                        </div>
                        <div className={"control"}>
                          <button
                            className={
                              "button is-rounded is-danger tooltip is-tooltip-bottom"
                            }
                            data-tooltip={"Discard"}
                            onClick={confirmDiscard}
                          >
                            <span className={"icon"}>
                              <ion-icon name="trash" />
                            </span>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </article>
            </div>
          </div>
          <div>
            <table
              className={"table is-bordered is-striped is-narrow is-hoverable"}
            >
              <thead>
                <tr>
                  <th className="is-danger">{"Delete"}</th>
                  <th className={"is-dark"}>{"Device"}</th>
                  <th className={"is-dark"}>{"Command"}</th>
                </tr>
              </thead>
              <tbody>{commandsList}</tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default NewSequenceTable;
