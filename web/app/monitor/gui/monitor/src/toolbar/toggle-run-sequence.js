import React from "react";

function ToggleRunSequence(props) {
  const iconName = props.isRunningSequence ? "square" : "play";

  return (
    <div className={"control"}>
      <button
        className={"button is-rounded is-warning tooltip is-tooltip-bottom"}
        data-tooltip={"Run selected sequence"}
        onClick={props.toggleRunningSequence}
        disabled={props.disabled}
      >
        <span className={"icon"}>
          <ion-icon name={iconName} />
        </span>
      </button>
    </div>
  );
}

export default ToggleRunSequence;
