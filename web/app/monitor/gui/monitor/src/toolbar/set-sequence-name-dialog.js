import React from "react";

class SetSequenceNameDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modal: props.active ? "modal is-active" : "modal",
      sequences: props.sequences,
      newSequenceName: "",
    };
    this.onChange = this.onChange.bind(this);
    this.submit = this.submit.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  onChange(event) {
    this.setState({
      newSequenceName: event.target.value,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    this._input.focus();
  }

  submit() {
    if (this.state.sequences.hasOwnProperty(this.state.newSequenceName)) {
      if (!confirm("The name already exists. Replace?")) {
        return;
      }
    }

    this.props.setNewSequenceName(this.state.newSequenceName);
    this.setState({
      modal: "modal",
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      sequences: nextProps.sequences,
      modal: nextProps.active ? "modal is-active" : "modal",
      newSequenceName: "",
    });
  }

  handleKeyPress(event) {
    switch (event.keyCode) {
      // escape key
      case 27:
        this.props.discardNewSequence();
        break;
      //Enter key
      case 13:
        this.submit();
        break;
      default:
        break;
    }
  }

  render() {
    return (
      <div className={this.state.modal} onKeyDown={this.handleKeyPress}>
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Set a new sequence's name</p>
          </header>

          <section className="modal-card-body">
            <div className="field">
              <div className="control">
                <input
                  id={"sequence-name"}
                  ref={(c) => (this._input = c)}
                  className={"input is-primary"}
                  type="text"
                  placeholder="Sequence name"
                  onChange={this.onChange}
                  value={this.state.newSequenceName}
                />
              </div>
            </div>
          </section>
          <footer className="modal-card-foot">
            <button
              className="button is-success"
              onClick={this.submit}
              disabled={!this.state.newSequenceName}
            >
              {"OK"}
            </button>
            <button
              className="button is-danger"
              onClick={this.props.discardNewSequence}
            >
              {"Cancel"}
            </button>
          </footer>
        </div>
      </div>
    );
  }
}

export default SetSequenceNameDialog;
