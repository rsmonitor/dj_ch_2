import React from "react";

function getTotalTime(sequenceToRun) {
  let totalTime = 0.0;

  for (let i = 0; i < sequenceToRun.length; i++) {
    let command = sequenceToRun[i];

    if (command.hasOwnProperty("pause")) {
      totalTime += parseFloat(command.pause);
    } else {
      totalTime += 0.05;
    }
  }

  return totalTime;
}

class SequenceRunner extends React.Component {
  constructor(props) {
    super(props);
    this.timer = 0;
    this.progressBarTick = 0;
    this.startTime = new Date();
    this._stateChecker = 0;

    this.state = {
      isRunningSequence: props.isRunningSequence,
      sequenceToRun: props.sequenceToRun,
      currentlyRunningCommand: -1,
      timer: undefined,
      totalTime: getTotalTime(props.sequenceToRun),
      progress: 0,
      isBackendAvailable: props.isBackendAvailable,
    };
    this.createCommandsList = this.createCommandsList.bind(this);
    this.runSequence = this.runSequence.bind(this);
    this.tickProgressBar = this.tickProgressBar.bind(this);
    this._runSequenceWhenStatusUpdated =
      this._runSequenceWhenStatusUpdated.bind(this);
  }

  componentWillUnmount() {
    this.setState({
      currentlyRunningCommand: -1,
      sequenceToRun: undefined,
      progress: 0,
    });
    this.props.setSequenceToRun(undefined);
    clearInterval(this.progressBarTick);
    clearTimeout(this.timer);
    clearInterval(this._stateChecker);
  }

  componentWillReceiveProps(nextProps) {
    const isBackendAvailable = nextProps.isBackendAvailable;

    this.setState({
      isBackendAvailable: isBackendAvailable,
      isRunningSequence: nextProps.isRunningSequence,
      sequenceToRun: nextProps.sequenceToRun,
    });

    if (!isBackendAvailable) {
      this.setState({
        currentlyRunningCommand: -1,
        progress: 0,
      });
      clearInterval(this.progressBarTick);
      clearTimeout(this.timer);
      clearInterval(this._stateChecker);
      return;
    }

    if (this.state.sequenceToRun !== nextProps.sequenceToRun) {
      this.setState({
        totalTime: getTotalTime(nextProps.sequenceToRun),
        progress: 0,
      });
    }

    if (this.state.isRunningSequence === nextProps.isRunningSequence) {
      return;
    }

    const isRunningSequence = this.state.isRunningSequence;

    if (!isRunningSequence) {
      this._stateChecker = this._runSequenceWhenStatusUpdated();
    } else {
      clearTimeout(this.timer);
      clearInterval(this.progressBarTick);
      this.setState({
        currentlyRunningCommand: -1,
        progress: 100,
      });
    }
  }

  tickProgressBar() {
    this.setState({
      progress:
        (this.state.currentlyRunningCommand * 100) /
        this.state.sequenceToRun.length,
    });
  }

  _runSequenceWhenStatusUpdated() {
    if (!this.state.isRunningSequence) {
      this._stateChecker = setTimeout(this._runSequenceWhenStatusUpdated, 10);
    } else {
      clearInterval(this._stateChecker);
      this.startTime = new Date();
      this.progressBarTick = setInterval(this.tickProgressBar, 50);
      setTimeout(this.runSequence, 0);
    }
  }

  runSequence(i = 0) {
    const sequence = this.state.sequenceToRun;
    if (!this.state.isRunningSequence) {
      clearInterval(this.progressBarTick);
      this.setState({
        progress: 100,
      });
      return;
    }

    if (i >= sequence.length) {
      if (this.props.endlessLoop) {
        this.setState({
          currentlyRunningCommand: -1,
          progress: 0,
        });
        clearInterval(this.progressBarTick);
        this._runSequenceWhenStatusUpdated();
        return;
      } else {
        this.setState({
          currentlyRunningCommand: -1,
          progress: 100,
        });
        this.props.toggleRunningSequence();
        clearInterval(this.progressBarTick);
        return;
      }
    }

    this.setState({
      currentlyRunningCommand: this.state.currentlyRunningCommand + 1,
    });

    let delay = 0;
    const command = sequence[i];

    if (command.hasOwnProperty("pause")) {
      delay = parseFloat(command.pause) * 1000;
    } else {
      const deviceId = command.device_id;

      if (!this.props.devices.hasOwnProperty(deviceId)) {
        this.props.toggleRunningSequence();
        this.setState({
          currentlyRunningCommand: -1,
          progress: 0,
        });
        clearTimeout(this.timer);
        clearInterval(this.progressBarTick);
        alert(`Unknown device:\n${deviceId}`);
        return;
      }

      command.request = "send_command_to_device";
      this.props.sendToBackend(command);
    }

    this.timer = setTimeout(() => {
      i++;
      this.runSequence(i);
    }, delay);
  }

  createCommandsList() {
    const sequence = this.state.sequenceToRun;
    const commandsList = Array();
    let command = undefined;
    let deviceCell = undefined;
    let commandCell = undefined;
    let rowStyle = undefined;

    for (let i = 0; i < sequence.length; i++) {
      command = sequence[i];
      rowStyle =
        this.state.isRunningSequence && this.state.currentlyRunningCommand === i
          ? "is-selected"
          : undefined;

      if (command.hasOwnProperty("pause")) {
        deviceCell = (
          <td>
            <span className={"icon"}>
              <ion-icon name="stopwatch" />
            </span>
          </td>
        );
        commandCell = <td>{command.pause}</td>;
      } else {
        deviceCell = (
          <td>
            <strong>{command.device_id}</strong>
          </td>
        );
        commandCell = <td>{command.command}</td>;
      }

      commandsList.push(
        <tr key={i} className={rowStyle}>
          {deviceCell}
          {commandCell}
        </tr>
      );
    }
    return commandsList;
  }

  render() {
    const commandsList = this.createCommandsList();

    return (
      <div className={"sequence-runner"}>
        <div className={"column is-narrow"}>
          <div className={"box"}>
            <div className="tile is-ancestor">
              <div className="tile is-parent">
                <article className="tile is-child notification is-danger">
                  <div className="content">
                    <div className={"title"}>
                      <div className={"field is-grouped"}>
                        <div className={"control"}>
                          <a
                            className={"delete"}
                            onClick={this.props.closeRunningSequence}
                          />
                        </div>
                        <div className={"control"}>
                          <h5 className={"title is-5"}>
                            {this.props.sequenceName}
                          </h5>
                        </div>
                      </div>
                    </div>
                  </div>
                </article>
              </div>
            </div>
            <div className={"box"}>
              <progress
                className={"progress is-info is-medium"}
                value={this.state.progress}
                max={"100"}
              />
              <div className={"field is-grouped"}>
                <div className={"control"}>
                  <strong>{parseInt(this.state.progress)}%</strong>
                </div>
              </div>
            </div>
            <div>
              <table
                className={
                  "table is-bordered is-striped is-narrow is-hoverable is-scrollable"
                }
              >
                <thead>
                  <tr>
                    <th>{"Device"}</th>
                    <th>{"Command"}</th>
                  </tr>
                </thead>
                <tbody className={"sequence-table"}>{commandsList}</tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SequenceRunner;
