import React from "react";

function ClearHistoryAll(props) {
  const className = props.hidden
    ? "button is-rounded is-black is-hidden"
    : "button is-rounded is-black";
  return (
    <div className={"control"}>
      <button
        className={className + " tooltip is-tooltip-bottom"}
        data-tooltip={"Clear history for all devices"}
        onClick={props.clearHistoryAll}
        disabled={props.disabled}
      >
        <span className={"icon"}>
          <ion-icon name="trash" />
        </span>
      </button>
    </div>
  );
}

export default ClearHistoryAll;
