import React from "react";
import StationName from "./toolbar/station-name";
import SequencePicker from "./toolbar/sequence-picker";
import RecordSequence from "./toolbar/record-sequence";
import ClearHistoryAll from "./toolbar/clear-history-all";
import MonitorAllPorts from "./toolbar/monitor-all-ports";
import StationSettings from "./toolbar/station-settings";
import BackendStatus from "./toolbar/backend-status";
import ToggleRunSequence from "./toolbar/toggle-run-sequence";
import ToggleEndlessLoop from "./toolbar/toggle-endless-loop";
import ToggleSequenceEditor from "./toolbar/toggle-sequence-editor";
import AgentStatus from "./toolbar/agent-status";

function TopBar(props) {
  const stationName = props.stationName;
  const sequences = props.sequences;
  const isRecordingSequence = props.isRecordingSequence;
  const isRunningSequence = props.isRunningSequence;
  const sequenceToRun = props.sequenceToRun;
  const isMonitoringAllPorts = props.isMonitoringAllPorts;
  const isBackendAvailable = props.isBackendAvailable;

  return (
    <nav className={"navbar is-fixed-top is-link"} id={"top-bar"}>
      <StationName stationName={stationName} />
      <div className={"navbar-item"}>
        <div className={"field is-grouped"}>
          <div className={"columns"}>
            <div className={"column is-one-fifths"}>
              <div className={"field has-addons"}>
                <SequencePicker
                  sequences={sequences}
                  setSequenceToRun={props.setSequenceToRun}
                  disabled={
                    isRecordingSequence ||
                    isRunningSequence ||
                    !isBackendAvailable
                  }
                  sequenceToRun={sequenceToRun}
                />
                <ToggleRunSequence
                  toggleRunningSequence={props.toggleRunningSequence}
                  isRunningSequence={props.isRunningSequence}
                  disabled={!props.sequenceToRun || !isBackendAvailable}
                />
                <ToggleEndlessLoop
                  toggleEndlessLoop={props.toggleEndlessLoop}
                  endlessLoop={props.endlessLoop}
                  disabled={
                    isRecordingSequence || !sequenceToRun || !isBackendAvailable
                  }
                />
                <ToggleSequenceEditor
                  disabled={
                    isRecordingSequence || !sequenceToRun || !isBackendAvailable
                  }
                  toggleEditorMode={props.toggleEditorMode}
                />
                <RecordSequence
                  isRecordingSequence={props.isRecordingSequence}
                  toggleRecordingSequence={props.toggleRecordingSequence}
                  disabled={
                    isRunningSequence || isRecordingSequence || sequenceToRun
                  }
                  discardNewSequence={props.discardNewSequence}
                  isNewSequenceNameDialogActive={
                    props.isNewSequenceNameDialogActive
                  }
                  sequences={props.sequences}
                  sequenceToRun={props.sequenceToRun}
                  setNewSequenceName={
                    props.setNewSequenceName || !isBackendAvailable
                  }
                />
                <ClearHistoryAll
                  clearHistoryAll={props.clearHistoryAll}
                  disabled={
                    isRecordingSequence ||
                    !isBackendAvailable ||
                    !props.historyNonEmpty
                  }
                />
                <MonitorAllPorts
                  isMonitoringAllPorts={isMonitoringAllPorts}
                  toggleMonitoringAllPorts={props.toggleMonitoringAllPorts}
                  disabled={
                    isRecordingSequence ||
                    isRunningSequence ||
                    sequenceToRun ||
                    !isBackendAvailable
                  }
                />
                <StationSettings
                  stationSettings={props.stationSettings}
                  disabled={
                    isRecordingSequence || isRunningSequence || sequenceToRun
                  }
                />
                <BackendStatus isBackendAvailable={props.isBackendAvailable} />
                <AgentStatus
                  agentConnected={props.isAgentConnected}
                  isBackendAvailable={props.isBackendAvailable}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className={"navbar-end"}>
        <div className={"navbar-item"}>
          <a className={"button is-success"} href={"/help"}>
            {"Help"}
          </a>
        </div>
        <div className={"navbar-item"}>
          <a className={"button is-success"} href={"/custom_logout"}>
            {"Log out"}
          </a>
        </div>
      </div>
    </nav>
  );
}

export default TopBar;
