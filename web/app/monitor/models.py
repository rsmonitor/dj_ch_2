from django.db import models
from django.contrib.auth.admin import User
import datetime


class PaidLicense(models.Model):
    registration_time = models.DateTimeField(auto_now=True)
    expiration_time = models.DateTimeField(auto_now=False)
    PAYMENT_METHODS = (
        ('PP', 'PayPal'),
        ('CC', 'Credit Card'),
    )
    payment_method = models.CharField(
        max_length=50,
        blank=False,
        null=False,
        choices=PAYMENT_METHODS
    )
    license_hash = models.CharField(max_length=500, null=False, blank=False)
    number_of_devices = models.IntegerField(null=False, blank=False, default=1)

    def is_active(self):
        return True
        registration = self.registration_time.replace(tzinfo=None)
        now = datetime.datetime.now()
        expiry = self.expiration_time.replace(tzinfo=None)

        if registration <= now < expiry:
            return True
        else:
            return False

    def __str__(self):
        status = 'Active' if self.is_active() else 'Not active'
        return f'Hash: {self.license_hash[:3]}... ({status}, {self.customer})'


class Customer(models.Model):
    manager = models.OneToOneField(User, on_delete=models.CASCADE)
    company_name = models.TextField(unique=True, max_length=200)
    license = models.OneToOneField(PaidLicense, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.company_name} (Mgr: {self.manager})'


class Group(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    name = models.TextField(max_length=40)

    def save(self, *args, **kwargs):
        if self.name in [grp.name for grp in self.customer.group_set.all()]:
            msg = f'Group {self.name} already exists for ({self.customer})'
            raise ValueError(msg)
        else:
            super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.name}'


class Operator(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user} ({self.group})'


class Station(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    name = models.TextField(max_length=40)
    ip = models.GenericIPAddressField()
    os = models.CharField(max_length=100, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.name in [stat.name for stat in self.group.station_set.all()]:
            msg = f'Station {self.name} already exists in ({self.group})'
            raise ValueError(msg)
        else:
            super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.name} ({self.ip}) ({self.group})'


class Port(models.Model):
    PROTOCOLS = (
        ('RS232', 'RS232'),
        ('RS485', 'RS485'),
    )
    BYTESIZES = (
        (5, 'FIVEBITS'),
        (6, 'SIXBITS'),
        (7, 'SEVENBITS'),
        (8, 'EIGHTBITS'),
    )
    BAUDRATES = ((b, str(b)) for b in [
        50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600,
        19200, 38400, 57600, 115200, 230400, 460800, 500000, 576000, 921600,
        1000000, 1152000, 1500000, 2000000, 2500000, 3000000, 3500000, 4000000
    ])
    PARITIES = (
        ('N', 'PARITY_NONE'),
        ('E', 'PARITY_EVEN'),
        ('O', 'PARITY_ODD'),
        ('M', 'PARITY_MARK'),
        ('S', 'PARITY_SPACE'),
    )
    STOPBITS = (
        (1,     'STOPBITS_ONE'),
        (1.5,   'STOPBITS_ONE_POINT_FIVE'),
        (2,     'STOPBITS_TWO')
    )
    station = models.ForeignKey(Station, on_delete=models.CASCADE)
    port_os_id = models.CharField(max_length=100, blank=False, null=False)
    baudrate = models.IntegerField(choices=BAUDRATES, default=115200)
    bytesize = models.IntegerField(choices=BYTESIZES, default=8)
    parity = models.CharField(max_length=1, choices=PARITIES, default='N')
    stopbits = models.FloatField(choices=STOPBITS, default=1)
    timeout = models.FloatField(default=0.5)
    xonxoff = models.BooleanField(default=False)
    rtscts = models.BooleanField(default=False, verbose_name='RTS/CTS')
    dsrdtr = models.BooleanField(default=False)
    write_timeout = models.FloatField(default=1.0)
    inter_byte_timeout = models.FloatField(default=1.0)
    exclusive = models.NullBooleanField(null=True, default=True)

    def save(self, *args, **kwargs):
        if self.port_os_id in [
            port.port_os_id for port in self.station.port_set.all()
        ]:
            msg = f'Port {self.port_os_id} already exists on ({self.station})'
            raise ValueError(msg)
        else:
            super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.port_os_id}, {self.station}'


class ListOfCommands(models.Model):
    name = models.CharField(max_length=100)
    port = models.ForeignKey(Port, on_delete=models.CASCADE)
    operators = models.ManyToManyField(Operator)

    def __str__(self):
        return f'{self.port}, {self.name}, {self.operators}'


class Command(models.Model):
    cmd = models.TextField(max_length=200, blank=False, null=False)
    name = models.TextField(max_length=30)
    port = models.ForeignKey(Port, on_delete=models.CASCADE)
    list_of_commands = models.ForeignKey(
        ListOfCommands,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f'{self.name} (Raw command: {self.cmd}) ({self.port})'


class CommandsSequence(models.Model):
    station = models.ForeignKey(Station, on_delete=models.CASCADE)
    name = models.TextField(max_length=50)
    sequence = models.TextField(max_length=100000, blank=False, null=False)

    def save(self, *args, **kwargs):
        sequences = [s.name for s in self.station.commandssequence_set.all()]

        if self.name in sequences:
            msg = f'Sequence {self.name} already exists for ({self.station})'
            raise ValueError(msg)
        else:
            super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.name} ({self.station})'
