from django.db import migrations
from django.contrib.auth.hashers import make_password
from datetime import date
from uuid import uuid4
from random import shuffle, random, randint, choice
import json

NUM_OF_COMPANIES = 3
NUM_OF_GROUPS = 3
NUM_OF_OPERATORS = 5
NUM_OF_STATIONS = 5
NUM_OF_PORTS = 3
NUM_OF_COMMANDS = 5
NUM_OF_SEQUENCES = 4
DEFAULT_OPERATOR_PASSWORD = '123456789'

DEVICES_NAMES = [
    'Sniffer',
    'PowerSupply',
    'SpectrumAnalyzer',
    'SwitchBoard',
    'Recorder',
    'DUT'
]


def create_commands_sequence(ports):
    sequence = []

    for p in ports:
        sequence.append({
            'pause': 0.5
        })
        sequence.append({
            'device_id': p,
            'command': ':IDN:?;'
        })

    return json.dumps(sequence)


def create_default_test_data(
        User,
        PaidLicense,
        Customer,
        Group,
        Operator,
        Station,
        Port,
        ListOfCommands,
        Command,
        CommandsSequence
    ):
    paid_license = PaidLicense(
        expiration_time=date(2020, 1, 1),
        payment_method='PP',
        license_hash='44f12ffd-8b0e-4df6-b460-aa0a6853ed46',
        number_of_devices=100
    )
    paid_license.save()
    company_name = 'butovski'
    manager = User(
        username='efi_butovski',
        email='efikoman@butovski.com',
        password=make_password(DEFAULT_OPERATOR_PASSWORD)
    )
    manager.save()
    customer = Customer(
        manager_id=manager.pk,
        company_name=company_name,
        license_id=paid_license.pk
    )
    customer.save()
    group = Group(customer=customer, name='QA_Test')
    group.save()
    operators = []

    for o in range(NUM_OF_OPERATORS):
        operator_name = f'operator_{o}'
        email = f'{operator_name}@{company_name}.com'
        user = User(
            username=operator_name,
            email=email,
            password=make_password(DEFAULT_OPERATOR_PASSWORD)
        )
        user.save()
        operator = Operator(user=user, group=group)
        operator.save()
        operators.append(operator)

    for i in range(NUM_OF_STATIONS):
        station = Station(
            group=group,
            name=f'station_{i}',
            ip=f'10.0.0.{i}',
            os='Linux'
        )
        station.save()
        ports = []

        for k in range(1, NUM_OF_PORTS + 1):
            for device_name in DEVICES_NAMES:
                port = Port(station=station, port_os_id=f'{device_name}_{k}')
                port.save()
                ports.append(port.port_os_id)

                for device_name in DEVICES_NAMES:
                    name = f'{device_name}'
                    loc = ListOfCommands(name=name, port=port)
                    loc.save()

                    for operator in operators:
                        loc.operators.add(operator)

                    for j in range(NUM_OF_COMMANDS):
                        for _ in range(3):
                            Command(
                                cmd=f'{device_name.upper()}:IDN:{j};',
                                name=f'Command_{j}',
                                port=port,
                                list_of_commands=loc
                            ).save()

        for s in range(NUM_OF_SEQUENCES):
            CommandsSequence(
                station=station,
                name=f'Sequence_{s}',
                sequence=create_commands_sequence(ports)
            ).save()


def populate_db(apps, schema_editor):
    User = apps.get_registered_model('auth', 'User')
    PaidLicense = apps.get_registered_model('monitor', 'PaidLicense')
    Customer = apps.get_registered_model('monitor', 'Customer')
    Group = apps.get_registered_model('monitor', 'Group')
    Operator = apps.get_registered_model('monitor', 'Operator')
    Station = apps.get_registered_model('monitor', 'Station')
    Port = apps.get_registered_model('monitor', 'Port')
    ListOfCommands = apps.get_registered_model('monitor', 'ListOfCommands')
    Command = apps.get_registered_model('monitor', 'Command')
    CommandsSequence = apps.get_registered_model('monitor', 'CommandsSequence')
    # Creating site admin
    admin_user = User(
        username='efi',
        email='efibutov@gmail.com',
        password=make_password('demesaorg'),
        is_superuser=True, is_staff=True
    )
    admin_user.save()
    create_default_test_data(
        User,
        PaidLicense,
        Customer,
        Group,
        Operator,
        Station,
        Port,
        ListOfCommands,
        Command,
        CommandsSequence
    )

    for i in range(NUM_OF_COMPANIES):
        paid_license = PaidLicense(
            expiration_time=date(2020, 1, 1),
            payment_method='PP',
            license_hash=uuid4(),
            number_of_devices=100
        )
        paid_license.save()
        company_name = f'company_{i}'

        manager = User(
            username=f'manager_{i}',
            email=f'manager_{i}@{company_name}.com',
            password=make_password(DEFAULT_OPERATOR_PASSWORD)
        )
        manager.save()

        customer = Customer(
            manager_id=manager.pk,
            company_name=company_name,
            license_id=paid_license.pk
        )
        customer.save()
        group = Group(customer=customer, name=f'group_{i}')
        group.save()

        operators = []

        for o in range(NUM_OF_OPERATORS):
            operator_name = f'operator_{o}_{company_name}'
            email = f'{operator_name}@{company_name}.com'
            user = User(
                username=operator_name,
                email=email,
                password=make_password(DEFAULT_OPERATOR_PASSWORD)
            )
            user.save()
            operator = Operator(user=user, group=group)
            operator.save()
            operators.append(operator)

        for j in range(NUM_OF_STATIONS):
            station = Station(
                group=group,
                name=f'station_{j}',
                ip=f'10.0.0.{j}',
                os='Linux'
            )
            station.save()
            ports = []

            for k in range(NUM_OF_PORTS):
                port = Port(
                    station=station,
                    port_os_id=f'{choice(DEVICES_NAMES)}_{k}'
                )
                port.save()
                ports.append(port.port_os_id)

                for device_name in DEVICES_NAMES:
                    name = f'{device_name}'
                    loc = ListOfCommands(name=name, port=port)
                    loc.save()

                    for operator in operators:
                        loc.operators.add(operator)

                    for c in range(NUM_OF_COMMANDS):
                        for _ in range(3):
                            Command(
                                cmd=f'{device_name.upper()}:IDN:{j};',
                                name=f'Command_{j}',
                                port=port,
                                list_of_commands=loc
                            ).save()

            for s in range(NUM_OF_SEQUENCES):
                CommandsSequence(
                    station=station,
                    name=f'Sequence_{s}',
                    sequence=create_commands_sequence(ports)
                ).save()


class Migration(migrations.Migration):
    dependencies = [('monitor', '0001_initial')]
    operations = [migrations.RunPython(populate_db),]
