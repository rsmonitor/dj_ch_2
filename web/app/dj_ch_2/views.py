from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from .forms import LoginForm, NewUserForm
from monitor.models import User, Group, Operator


def create_random_accounts(request):
    if False:
        users = [user.email for user in User.objects.all()]
        return HttpResponse('<ul>' + '<li>'.join(users) + '</ul>')
    else:
        import os
        import random
        cur_dir = os.path.dirname(os.path.realpath(__file__))

        with open(os.path.join(cur_dir, 'tests', 'first-names.txt'), 'r') as _:
            f_names = _.read().split()

        with open(os.path.join(cur_dir, 'tests', 'last-names.txt'), 'r') as _:
            l_names = [l_name.lower().capitalize() for l_name in _.read().split()]

        with open(os.path.join(cur_dir, 'tests', 'free_email_provider_domains.txt'), 'r') as _:
            domains = _.read().split()

        users = []

        for i in range(1000):
            first_name = random.choice(f_names)
            first_name_lower = first_name.lower()
            last_name = random.choice(l_names)
            last_name_lower = last_name.lower()
            domain = random.choice(domains)
            email = f'{first_name_lower}_{last_name_lower}@{domain}'
            users.append(
                User(
                    username=f'{first_name_lower}_{last_name_lower}',
                    first_name=first_name,
                    last_name=last_name,
                    email=email,
                    password='123456'
                )
            )

        status = User.objects.bulk_create(users)
        return HttpResponse(f'{status}')


def testing(request):
    return render(request, 'dj_ch_2/testing.html')


def help(request):
    return render(request, 'dj_ch_2/help.html')


def create_new_license(request):
    request.user.username


def sent_email_notification(request, email):
    return HttpResponse(email)


def send_registration_email(username, email):
    pass


def create_trial_account(request):
    if request.method == 'POST':
        form = NewUserForm(request.POST)

        if form.is_valid():
            username = form.data['username']
            email = form.data['email']

            send_registration_email(username, email)
            return render(
                request,
                'dj_ch_2/email_sent_notification.html',
                {'email': email, 'username': username}
            )
    else:
        form = NewUserForm()

    return render(request, 'dj_ch_2/create_trial_account.html', {'form': form})


def create_business_account(request):
    if request.method == 'POST':
        form = NewUserForm(request.POST)

        if form.is_valid():
            return HttpResponseRedirect('/thanks/')
    else:
        form = NewUserForm()

    return render(
        request,
        'dj_ch_2/create_business_account.html',
        {'form': form}
    )


def custom_logout(request):
    logout(request)
    return HttpResponseRedirect('/custom_login/')


@login_required(login_url='/custom_login/')
def account(request):
    operator = Operator.objects.get(user=request.user)
    customer = operator.customer
    groups = Group.objects.filter(customer=customer)
    return render(
        request,
        'dj_ch_2/account.html',
        {
            'user': request.user,
            'groups': groups
        }
    )


def custom_login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/monitor/operator_home/')
        # todo: Error message
        form = LoginForm(request.POST)
    else:
        form = LoginForm()

    return render(
        request,
        'dj_ch_2/custom_login.html',
        {'login_form': form}
    )


def home(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/monitor/operator_home/')
    else:
        return HttpResponseRedirect('/custom_login/')
