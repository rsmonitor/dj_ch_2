from django.conf.urls import url, include
from django.contrib import admin
from dj_ch_2 import views


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^account/$', views.account, name='account'),
    url(r'^create_random_accounts/$', views.create_random_accounts, name='create_random_accounts'),
    url(r'^custom_login/$', views.custom_login, name='custom_login'),
    url(r'^custom_logout/$', views.custom_logout, name='custom_logout'),
    url(r'^create_trial_account/$', views.create_trial_account, name='create_trial_account'),
    url(r'^create_business_account/$', views.create_business_account, name='create_business_account'),
    url(r'^testing/$', views.testing, name='testing'),
    url(r'^help/', views.help, name='help'),
    url(r'^sent_email_notification/(?P<email>.+)/$', views.sent_email_notification, name='sent_email_notification'),
    url(r'^admin/', admin.site.urls),
   url(r'^monitor/', include('monitor.urls')),
]
