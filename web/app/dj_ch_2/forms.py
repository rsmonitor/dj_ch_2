from django import forms
from monitor.models import User


class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100)


class NewUserForm(forms.Form):
    username = forms.CharField(label='Username', max_length=20)
    email = forms.EmailField()
    password = forms.CharField(max_length=32, widget=forms.PasswordInput())
    password2 = forms.CharField(max_length=32, widget=forms.PasswordInput())

    def clean_username(self):
        username = self.cleaned_data['username']
        print(username)

        if User.objects.filter(username=username):
            raise forms.ValidationError('This username already exists')

        return username

    def clean_email(self):
        email = self.cleaned_data['email']
        print(email)

        if User.objects.filter(email=email):
            raise forms.ValidationError('This email already exists')

        return email

    def clean_password(self):
        password = self.cleaned_data['password']

        if len(password) < 8:
            raise forms.ValidationError('Too short password')

        return password

    def clean_password2(self):
        password2 = self.cleaned_data['password2']

        if self.cleaned_data['password'] != password2:
            raise forms.ValidationError('Passwords do not match')

        return password2


class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=20)
    password = forms.CharField(max_length=32, widget=forms.PasswordInput())
    login_as_manager = forms.BooleanField(required=False, label='Log me in as a manager', initial=False)
