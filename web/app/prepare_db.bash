#!/usr/bin/env bash

rm /app/monitor/migrations/0001_initial.py
mv /app/monitor/migrations/fixture.py /app/monitor/migrations/fixture
python /app/manage.py makemigrations
mv /app/monitor/migrations/fixture /app/monitor/migrations/fixture.py
python /app/manage.py migrate

echo "\n=======================================================\n"
echo "\n\nFINISHED MIGRATING\n\n"
echo "\n=======================================================\n"

# Process requests from the localhost only (from Nginx)
# Nginx run in this config on the localhost only
#python /app/manage.py runserver 0.0.0.0:80
