Install those libs:
sudo apt-get install python3-dev
sudo apt-get install libevent-dev

==========================================

Running redis (as message queue)

docker run -p 6379:6379 -d redis:2.8
==========================================

Install psql client:
sudo apt-get install postgresql-client

==========================================

Run docker:
mkdir /home/efi/dat2
docker kill $(docker ps -aq); docker rm $(docker ps -aq); docker run -v /home/efi/data2:/var/lib/postgresql/data -it -p 5432:5432 --name p_m_postgres -e POSTGRES_PASSWORD=demesaorg -d postgres

==========================================

Connect to Postgres:
psql -h localhost -p 5432 -d postgres -U postgres --password

==========================================

Create database:

CREATE DATABASE port_monitor;
CREATE USER efi WITH PASSWORD 'demesaorg';
GRANT ALL PRIVILEGES ON DATABASE port_monitor TO efi;
ALTER USER efi CREATEDB;

==========================================

docker-compose up --build; docker-compose stop;  docker-compose run --rm web /bin/bash -c "/app/prepare_db.bash"
docker-compose run --rm web /bin/bash -c "/app/prepare_db.bash"